<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

/**
 * Class CreateAgentLevelTable
 * 代理层级详情
 */
class CreateAgentLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_level', function (Blueprint $table) {
            $table->comment = '代理等级表';
            $table->increments('id');
            $table->integer('levelNo')->comment('层级编号')->index();
            $table->string('title')->comment('层级名称');
            $table->tinyInteger('timeType')->default(1)->comment('佣金周期 0日 1周 2月');
            $table->decimal('minAmount', 24, 8)->default('0.00')->comment('最小佣金额度');
            $table->decimal('maxAmount', 24, 8)->default('0.00')->comment('最大佣金额度');
            $table->tinyInteger('status')->default(1)->comment('层级状态 0未启用 1启用');
            $table->string('remark')->nullable()->comment('层级说明');
            $table->integer('adminId')->comment('操作人ID');
            $table->string('adminName', 20)->comment('操作人姓名');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('agent_level');
    }
}
