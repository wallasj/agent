<?php

//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateBankTable
 * 银行管理
 */
class CreateBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->comment = '银行管理';
            $table->bigIncrements('id');
            $table->string('bankName', '50')->comment('银行名称');
            $table->string('bankCode', '20')->comment('银行编号');
            $table->string('bankImg', '255')->comment('银行图标');
            $table->tinyInteger('status')->default(1)->comment('状态 0未启用 1启用');
            $table->tinyInteger('type')->default(1)->comment('类型 1出款银行 2入款银行');
            $table->integer('adminId')->nullable()->comment('操作人ID');
            $table->string('adminName', 20)->nullable()->comment('操作人姓名');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('bank');
    }
}
