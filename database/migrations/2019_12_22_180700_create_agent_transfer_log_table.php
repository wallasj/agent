<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateAgentTransferLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_transfer_log', function (Blueprint $table) {
            $table->comment = '代理转账表';
            $table->bigIncrements('id')->comment('id');
            $table->integer('agentId')->comment('代理ID')->index();
            $table->integer('toAgentId')->nullable()->comment('到账代理ID')->index();
            $table->string('toAgentName', 30)->nullable()->comment('到账代理名称')->index();
            $table->integer('toMemberId')->nullable()->comment('到账用户ID')->index();
            $table->string('toMemberName', 30)->nullable()->comment('到账用户名称')->index();
            $table->decimal('money', 24, 8)->comment('转账金额');
            $table->decimal('beforeAgentMoney', 24, 8)->comment('转账前代理金额');
            $table->decimal('afterAgentMoney', 24, 8)->comment('转账后代理金额');
            $table->decimal('beforeMemberMoney', 24, 8)->comment('转账前用户金额');
            $table->decimal('afterMemberMoney', 24, 8)->comment('转账后用户金额');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('agent_commission_statistics');
    }
}
