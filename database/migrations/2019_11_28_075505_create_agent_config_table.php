<?php

//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAgentInfoTable
 * 代理信息表
 */
class CreateAgentConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_config', function (Blueprint $table) {
            $table->comment = '代理设置';
            $table->integerIncrements('id')->comment('默认ID');
            $table->string('defaultUrl')->nullable()->comment('默认推广链接');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('agent_config');
    }
}
