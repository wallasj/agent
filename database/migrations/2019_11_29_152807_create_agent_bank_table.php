<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

/**
 * Class CreateAgentBankTable
 * 代理银行卡管理
 */
class CreateAgentBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_bank', function (Blueprint $table) {
            $table->comment = '代理会员银行卡';
            $table->integerIncrements('id')->comment('默认ID');
            $table->integer('agentId')->comment('代理ID')->index();
            $table->tinyInteger('isMain')->default(0)->comment('是否为主卡 0否 1是');
            $table->tinyInteger('bankId')->comment('银行卡ID');
            $table->string('bankCode', 20)->comment('银行编号');
            $table->string('bankName', 30)->comment('银行名称');
            $table->string('bankAccount', 50)->comment('银行卡号');
            $table->string('bankUserName', 30)->comment('开户人');
            $table->string('bankBranchName', 100)->nullable()->comment('支行地址');
            $table->tinyInteger('status')->default(0)->comment('银行卡状态 0禁用 1启用');
            $table->string('province', 30)->nullable()->comment('开户省');
            $table->string('city', 30)->nullable()->comment('开户市');
            $table->string('area', 30)->nullable()->comment('开户区');
            $table->tinyInteger('bankCount')->comment('银行卡总数');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('agent_bank');
    }
}
