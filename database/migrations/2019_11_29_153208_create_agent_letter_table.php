<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

/**
 * Class CreateAgentLetterTable
 * 代理站内信
 */
class CreateAgentLetterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_letter', function (Blueprint $table) {
            $table->comment = '代理站内信表';
            $table->bigIncrements('id');
            $table->string('title', 50)->comment('标题');
            $table->text('content')->comment('信件内容');
            $table->tinyInteger('type')->default(0)->comment('信件类型 0系统信件 1用户信件');
            $table->integer('agentId')->nullable()->comment('收件人');
            $table->integer('adminId')->comment('操作人ID');
            $table->string('adminName', 20)->comment('操作人姓名');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('agent_letter');
    }
}
