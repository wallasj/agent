<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

/**
 * Class CreateAgentCommissionStatisticsTable
 * 代理 - 佣金报表
 */
class CreateAgentCommissionStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_commission_statistics', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->comment = '代理佣金统计表';
            $table->bigIncrements('id');
            $table->bigInteger('agentId')->comment('代理ID')->index();
            $table->decimal('balance', 24, 8)->default('0.00')->comment('账户余额');
            $table->string('agentName', 30)->comment('代理名称');
            $table->string('proposalNo', 32)->comment('佣金提案编号')->unique();
            $table->string('commissionTime', 20)->comment('佣金期号');
            $table->tinyInteger('commissionType')->comment('佣金方案');
            $table->string('commissionName', 30)->comment('佣金方案名称');
            $table->decimal('lastSurplus', 24, 8)->default('0.00')->comment('上期结余');
            $table->decimal('commissionAmount', 24, 8)->default('0.00')->comment('本期佣金');
            $table->tinyInteger('status')->default(0)->comment('佣金状态');
            $table->integer('activeMember')->default(0)->comment('活跃会员数');
            $table->decimal('validAmount', 24, 8)->comment('有效投注额')->default('0.00');
            $table->decimal('payoutAmount', 24, 8)->default('0.00')->comment('总派彩');
            $table->decimal('promoteAmout', 24, 8)->default('0.00')->comment('下线总优惠');
            $table->decimal('profitAmount', 24, 8)->default('0.00')->comment('公司总盈利');
            $table->decimal('actualCommission', 24, 8)->default('0.00')->comment('实际可领佣金');
            $table->string('offlineCost', 30)->nullable()->comment('线下运营成本');
            $table->string('bankRate', 30)->nullable()->comment('银行卡手续费');
            $table->string('otherCost', 30)->nullable()->comment('其他成本');
            $table->string('remark', 200)->nullable()->comment('备注说明');
            $table->float('rate')->nullable()->comment('佣金比例')->default(0);
            $table->timestamp('beginTime')->nullable()->comment('结算周期开始');
            $table->timestamp('endTime')->nullable()->comment('结算周期结束');
            $table->timestamp('auditTime')->nullable()->comment('佣金审核时间');
            $table->string('auditIp', 30)->nullable()->comment('审核IP');
            $table->integer('adminId')->comment('操作人ID');
            $table->string('adminName', 20)->comment('操作人姓名');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('agent_commission_statistics');
    }
}
