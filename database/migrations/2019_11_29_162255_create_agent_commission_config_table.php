<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

/**
 * Class CreateAgentCommissionConfigTable
 * 代理佣金设置
 */
class CreateAgentCommissionConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_commission_config', function (Blueprint $table) {
            $table->comment = '代理配置表';
            $table->bigIncrements('id')->comment('自增ID');
            $table->integer('commissionId')->comment('佣金ID')->index();
            $table->string('fieldName', 30)->comment('配置名称');
            $table->tinyInteger('type')->default(0)->comment('配置类型 0状态 1字段');
            $table->tinyInteger('status')->nullable()->default(0)->comment('配置是否启用 - 类型为状态时启用 0否 1是');
            $table->tinyInteger('required')->nullable()->default(0)->comment('是否必填 - 类型为字段时启用 0否 1是');
            $table->tinyInteger('visible')->nullable()->default(0)->comment('是否可见 - 类型为字段时启用 0否 1是');
            $table->integer('adminId')->comment('操作人ID');
            $table->string('adminName', 20)->comment('操作人姓名');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('agent_commission_config');
    }
}
