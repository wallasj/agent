<?php

//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAgentTable
 * 代理用户表
 */
class CreateAgentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->comment = '代理用户表';
            $table->bigIncrements('id');
            $table->bigInteger('commissionId')->default(0)->comment('佣金ID')->index();
            $table->string('agentNo', 32)->comment('代理编号');
            $table->string('agentName', 30)->unique()->comment('代理账号');
            $table->string('password', 120)->comment('代理密码');
            $table->string('payword', 120)->comment('支付密码');
            $table->string('realName', 60)->comment('真实姓名');
            $table->decimal('wallet', 24, 8)->default('0.00')->comment('佣金钱包');
            $table->tinyInteger('level')->default(0)->comment('代理等级');
            $table->tinyInteger('realNameValid')->default(0)->comment('真实姓名验证情况 0未验证 1验证中 2已验证');
            $table->string('phone', 50)->unique()->comment('电话号码');
            $table->tinyInteger('phoneValid')->default(0)->comment('电话号码验证情况 0未验证 1验证中 2已验证');
            $table->string('email', 80)->comment('邮箱');
            $table->tinyInteger('emailValid')->default(0)->comment('电话号码验证情况 0未验证 1验证中 2已验证');
            $table->string('registerSource', 10)->comment('注册登陆来源');
            $table->string('registerIp', 30)->comment('注册IP');
            $table->string('inviteCode', 30)->unique()->comment('推广码');
            $table->string('remark', 30)->nullable()->comment('备注');
            $table->tinyInteger('status')->default(0)->comment('账号状态 0禁用 1启用');
            $table->text('apiToken')->nullable()->comment('用户token');
            $table->integer('parent_id')->unsigned()->default(0)->index('idx_parentId')->comment('父级ID');
            $table->integer('left')->unsigned()->nullable()->index('idx_left')->comment('层级左值');
            $table->integer('right')->unsigned()->nullable()->index('idx_right')->comment('层级右值');
            $table->integer('depth')->unsigned()->nullable()->index('idx_depth')->comment('当前层级深度');
            $table->string('lastLoginSource')->nullable()->default('')->comment('最后登录来源');
            $table->string('lastLoginIp')->nullable()->default('')->comment('最后登录IP');
            $table->timestamp('lastLoginAt')->nullable()->comment('最后登录时间');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('agent');
    }
}
