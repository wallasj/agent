<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

/**
 * Class CreateAgentDayReportTable
 * 代理日报表
 */
class CreateAgentDayReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_day_report', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->comment = '代理会员日报表';
            $table->bigIncrements('id');
            $table->integer('agentId')->index()->comment('代理ID');
            $table->integer('agentPid')->nullable()->comment('代理PID');
            $table->string('agentNo', 32)->comment('代理编号');
            $table->string('agentName', 30)->comment('代理账号');
            $table->integer('registerCount')->default(0)->comment('注册会员');
            $table->integer('totalAccount')->default(0)->comment('开户总人数');
            $table->decimal('totalEarnings', 24 ,8)->default('0.00')->comment('盈利总额');
            $table->integer('totalSingle')->default(0)->comment('总注单数');
            $table->smallInteger('firstDeposit')->default(0)->comment('首存总数');
            $table->smallInteger('secondDeposit')->default(0)->comment('二存总数');
            $table->smallInteger('thirdDeposit')->default(0)->comment('三存总数');
            $table->decimal('depositAmount', 24, 8)->default('0.00')->comment('总存款');
            $table->decimal('withdrawAmount', 24, 8)->default('0.00')->comment('总提款');
            $table->float('dwRate')->default(0)->comment('存提比');
            $table->decimal('promoteAmount', 24, 8)->default('0.00')->comment('总优惠');
            $table->decimal('betAmount',24, 8)->default('0.00')->comment('总投注额');
            $table->decimal('betValidAmount',24, 8)->default('0.00')->comment('总有效投注');
            $table->integer('betValidPerson')->default(0)->comment('有效投注人数');
            $table->decimal('payoutAmount',24, 8)->default('0.00')->comment('总派彩');
            $table->decimal('winningsAmount',24, 8)->default('0.00')->comment('总彩金');
            $table->decimal('profitAmount', 24, 8)->default('0.00')->comment('总盈利');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('agent_day_report');
    }
}
