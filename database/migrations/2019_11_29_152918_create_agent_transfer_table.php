<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateAgentTransferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_transfer', function (Blueprint $table) {
            $table->comment = '代理会员转代记录表';
            $table->increments('id');
            $table->integer('agentId')->comment('代理ID')->index();
            $table->integer('beforePpid')->comment('转代前上级ID');
            $table->integer('afterPid')->comment('转代后上级ID');
            $table->string('remark')->nullable()->comment('备注');
            $table->integer('adminId')->comment('操作人ID');
            $table->tinyInteger('status')->default(0)->comment('状态');
            $table->string('adminName', 20)->comment('操作人姓名');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('agent_transfer');
    }
}
