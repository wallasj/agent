<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

/**
 * Class CreateAgentCommissionRuleTable
 * 佣金方案规则
 */
class CreateAgentCommissionRuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_commission_rule', function (Blueprint $table) {
            $table->comment = '代理佣金规则表';
            $table->integerIncrements('id')->comment('默认ID');
            $table->integer('commissionId')->comment('佣金ID')->index();
            $table->tinyInteger('commissionLevel')->comment('层级编号');
            $table->decimal('minAmount', 24, 8)->default('0.00')->comment('最小金额');
            $table->decimal('maxAmount', 24, 8)->default('0.00')->comment('最大金额');
            $table->float('rate')->comment('佣金比例');
            $table->integer('adminId')->comment('操作人ID');
            $table->string('adminName', 20)->comment('操作人姓名');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('agent_commission_rule');
    }
}
