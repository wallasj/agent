<?php

//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAgentInfoTable
 * 代理信息表
 */
class CreateAgentInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_info', function (Blueprint $table) {
            $table->comment = '代理会员信息表';
            $table->integer('agentId')->comment('代理ID')->unique();
            $table->timestamp('birthDay')->nullable()->comment('生日');
            $table->string('qq', 50)->nullable()->comment('QQ号');
            $table->string('wechat', 50)->nullable()->comment('微信号');
            $table->string('generalizeLinkMain')->nullable()->comment('主推广链接');
            $table->string('generalizeLinkSecond')->nullable()->comment('子推广链接');
            $table->string('generalizeLinkThird')->nullable()->comment('子推广链接');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('agent_info');
    }
}
