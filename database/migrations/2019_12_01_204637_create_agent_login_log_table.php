<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateAgentLoginLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_login_log', function (Blueprint $table) {
            $table->comment = '代理登录日志表';
            $table->bigInteger('agentId')->index();
            $table->string('agentName', 30)->comment('代理姓名');
            $table->string('agentNo', 32)->comment('代理编号');
            $table->string('loginIp', 30)->comment('最后登录IP');
            $table->string('source', 30)->comment('登陆来源');
            $table->text('token')->comment('token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('agent_login_log');
    }
}
