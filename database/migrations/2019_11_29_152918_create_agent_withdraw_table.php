<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateAgentWithdrawTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_withdraw', function (Blueprint $table) {
            $table->comment = '代理会员取款表';
            $table->increments('id');
            $table->integer('agentId')->comment('代理ID')->index();
            $table->decimal('money', 24, 8)->comment('取款金额');
            $table->decimal('beforeAmount', 24, 8)->default(0)->comment('取款前金额');
            $table->decimal('afterAmount', 24, 8)->default(0)->comment('取款后金额');
            $table->integer('bankId')->index()->comment('银行卡ID');
            $table->string('bankName', 20)->comment('银行卡名称');
            $table->string('bankCode', 20)->comment('银行卡编号');
            $table->string('bankAccount', 50)->comment('银行卡账号');
            $table->string('bankUserName', 30)->comment('开户人姓名');
            $table->string('remark')->nullable()->comment('备注');
            $table->tinyInteger('status')->default(0)->comment('取款状态 -1未通过 0提交中 1待审批 2已通过');
            $table->string('sources', 10)->nullable()->comment('来源 H5/PC/WAP');
            $table->string('ip', 20)->default(0)->comment('取款IP');
            $table->tinyInteger('types')->default(0)->comment('取款类型 1自发 2后台');
            $table->integer('adminId')->nullable()->comment('操作人ID');
            $table->string('adminName', 20)->nullable()->comment('操作人姓名');
            $table->timestamp('auditTime')->nullable()->comment('审核时间');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('agent_withdraw');
    }
}
