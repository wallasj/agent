<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

/**
 * Class CreateAgentCommissionTable
 * 代理佣金表
 */
class CreateAgentCommissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_commission', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->comment = '代理佣金详情表';
            $table->bigIncrements('id');
            $table->string('commissionName', 50)->comment('方案名称');
            $table->tinyInteger('commissionStatus')->default(0)->comment('方案状态 0未启用 1启用');
            $table->tinyInteger('commissionType')->default(1)->comment('佣金类型 0占成 1抽佣');
            $table->tinyInteger('isCunsum')->default(0)->comment('是否公司盈利累加 0否 1是');
            $table->tinyInteger('isDefault')->nullable()->default(0)->comment('是否是默认方案，每种类型只能存在一个');
            $table->integer('bindCount')->comment('绑定数量');
            $table->integer('validMember')->nullable()->default(0)->comment('有效会员数');
            $table->integer('validAmount')->nullable()->default(0)->comment('有效投注额');
            $table->tinyInteger('commissionTime')->nullable()->default(0)->comment('计算周期 0日结 1周结 2月结');
            $table->integer('adminId')->comment('操作人ID');
            $table->string('adminName', 20)->comment('操作人姓名');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('agent_commission');
    }
}
