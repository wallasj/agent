<?php

use Illuminate\Database\Seeder;

class AgentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Model\Agent::class)->times(config('database.seedMaxNumber'))->make()->each(function ($model) {
            $model->save();
        });
    }
}
