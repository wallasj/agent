<?php

use Illuminate\Database\Seeder;

class LetterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Model\Letter::class)->times(config('database.seedMaxNumber'))->make()->each(function ($model) {
            $model->save();
        });
    }
}
