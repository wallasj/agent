<?php

use Illuminate\Database\Seeder;

class AgentDayReportTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agent = \App\Model\Agent::doesnthave('agentDayReport')->where('status', 1)->get();

        if ($agent->isEmpty()) {
            factory(\App\Model\Agent::class, config('database.seedMaxNumber'))->make();
        }

        factory(\App\Model\AgentDayReport::class)->times(1)->make()->each(function ($model) {
            $model->save();
        });
    }
}
