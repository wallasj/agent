<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //生成代理
        $this->call(AgentTableSeeder::class);

        //生成佣金报表
        $this->call(AgentCommissionStatisticsTableSeeder::class);

        //生成佣金日报表
        $this->call(AgentDayReportTableSeeder::class);

        //生成站内信
        $this->call(LetterTableSeeder::class);
    }
}
