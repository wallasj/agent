<?php

use Illuminate\Database\Seeder;

class AgentCommissionStatisticsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agent = \App\Model\Agent::doesnthave('agentComissionStatistics')->where('status', 1)->get();

        if ($agent->isEmpty()) {
            factory(\App\Model\Agent::class, config('database.seedMaxNumber'))->create();
        }

        factory(\App\Model\AgentCommissionStatistics::class)->times(1)->make()->each(function ($model) {
            $model->save();
        });
    }
}
