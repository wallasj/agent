<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Agent;
use Faker\Generator as Faker;

$factory->define(Agent::class, function (Faker $faker) {
    return [
        'agentNo' => $faker->randomNumber(),
        'agentName' => $faker->firstName,
        'password' => doSecret('123456', 'password'),
        'payword' => doSecret('123456', 'password'),
        'realName' => doSecret($faker->name, 'username'),
        'phone' => doSecret('1' . $faker->numberBetween(1000000000, 9999999999), 'phone'),
        'email' => doSecret($faker->email, 'email'),
        'registerSource' => $faker->randomElement(config('dictionary.source')),
        'registerIp' => $faker->ipv4,
        'inviteCode' => createOrderNo('A'),
        'status' => 1
    ];
});

$factory->afterCreating(Agent::class, function ($agent) {
    $node = $agent->makeRoot();
    $node->parent_id = null;
    $node->save();
});

$factory->afterMaking(Agent::class, function ($agent) {
//    $node = $agent->makeRoot();
//    $node->parent_id = null;
//    $node->save();
});
