<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use \App\Model\Letter;
use Faker\Generator as Faker;

$factory->define(\App\Model\Letter::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'content' => $faker->sentence,
        'type' => $faker->biasedNumberBetween(0, 1),
        'adminId' => 1,
        'adminName' => 'system_auto'
    ];
});
