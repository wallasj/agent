<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\AgentDayReport;
use Faker\Generator as Faker;

$factory->define(AgentDayReport::class, function (Faker $faker) {

    $agent = \App\Model\Agent::doesnthave('agentDayReport')
        ->select('id', 'agentName', 'parent_id', 'agentNo')
        ->where('status', 1)
        ->orderByRaw('RAND()')
        ->first();

    if (!empty($agent)) {

        usleep(1000);

        return [
            'agentId' => $agent->id,
            'agentName' => $agent->agentName,
            'agentPid' => $agent->parent_id,
            'agentNo' => $agent->agentNo,
            'registerCount' => $faker->numberBetween(0, 100),
            'totalAccount' => $faker->numberBetween(0, 100),
            'totalEarnings' => $faker->randomFloat(),
            'totalSingle' => $faker->numberBetween(0, 100),
            'firstDeposit' => $faker->numberBetween(0, 100),
            'secondDeposit' => $faker->numberBetween(0, 100),
            'thirdDeposit' => $faker->numberBetween(0, 100),
            'depositAmount' => $faker->randomFloat(),
            'withdrawAmount' => $faker->randomFloat(),
            'dwRate' => $faker->randomFloat(null, 0 ,100),
            'promoteAmount' => $faker->randomFloat(),
            'betAmount' => $faker->randomFloat(),
            'betValidAmount' => $faker->randomFloat(),
            'betValidPerson' => $faker->numberBetween(0, 1000),
            'payoutAmount' => $faker->randomFloat(),
            'winningsAmount' => $faker->randomFloat(),
            'profitAmount' => $faker->randomFloat()
        ];

    }
});
