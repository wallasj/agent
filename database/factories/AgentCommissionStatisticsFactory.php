<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\AgentCommissionStatistics;
use Faker\Generator as Faker;

$factory->define(AgentCommissionStatistics::class, function (Faker $faker) {

    $agent = \App\Model\Agent::doesnthave('agentComissionStatistics')
                ->select('id', 'agentName')
                ->where('status', 1)
                ->orderByRaw('RAND()')
                ->first();

    if (!empty($agent)) {

        usleep(1000);

        return [
            'agentId' => $agent->id,
            'agentName' => $agent->agentName,
            'balance' => $faker->randomFloat(),
            'proposalNo' => createOrderNo('C'),
            'commissionTime' => date('Y-m-d'),
            'commissionType' => 1,
            'commissionName' => '测试生成',
            'lastSurplus' => $faker->randomFloat(),
            'commissionAmount' => $faker->randomFloat(),
            'activeMember' => $faker->numberBetween(),
            'payoutAmount' => $faker->randomFloat(),
            'promoteAmout' => $faker->randomFloat(),
            'profitAmount' => $faker->randomFloat(),
            'actualCommission' => $faker->randomFloat(),
            'adminId' => 1,
            'adminName' => 'system_auto'
        ];

    }
});
