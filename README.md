## 开发规范
    请在类中添加严格模式  
    - declare(strict_types=1);
    
## 测试数据
    //防止报错，自动重载
    composer dump-autoload
    
    //执行所有种子
    php artisan db:seed
    
    //生成文档
    php artisan apidoc:generate
    
    //代理用户
    php artisan db:seed --class=AgentTableSeeder
    
    //佣金报表
    php artisan db:seed --class=AgentCommissionStatisticsTableSeeder
    
    //佣金日报
    php artisan db:seed --class=AgentDayReportTableSeeder
    
    //站内信
    php artisan db:seed --class=LetterTableSeeder
    
    //生成数据字典
    php artisan vendor:publish
    
## 项目初始化

~~~~初始化请遵循以下步骤

- 初始化项目第三方扩展库 composer update

- 初始化数据库 php artisan migrate

- 初始化 APPKEY php artisan key:generate

- 初始化 JWT 配置 php artisan jwt:secret

- 复制 .env.example => .env 文件

- 增加权限
    - 项目权限 chmod 775 -R 项目名称
    - chmod 777 -R 项目名称 / storage

- 使用git忽略掉权限变化
    - 修改 .git/config => filemode = false

- 新增 .env 下如下配置
    - PASSWORD_PREFIX="dgtxPwd"
    - PHONE_PREFIX="dgtxPhone"
    - EMAIL_PREFIX="dgtxEmail"
    - REAL_NAME_PREFIX="dgtxRealName"
    - MEMBER_API=用户系统地址
    
- 修改原 .env 配置如下
    - APP_NAME=DGTX
    - APP_ENV=local 改为 testing 或 product
    - APP_DEBUG=false
    - APP_URL=域名
