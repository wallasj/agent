<?php
/**
 *
 * User: wallas
 * Date: 2019/12/2
 * Description:
 */

return [
    //状态
    'status' => [
        -1 => '禁用',
         0 => '审核中',
         1 => '启用'
    ],
    //站内信类型
    'letterType' => [
        '系统信件', '用户信件'
    ],
    //请求来源
    'source' => [
        'PC', 'H5', 'APP', 'IOS', 'ANDROID'
    ],
    //佣金状态
    'commissionType' => [
        -1 => '拒绝',
         0 => '待审核',
         1 => '已审核',
         2 => '已入账'
    ]
];
