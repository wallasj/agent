<?php
/**
 *
 * User: wallas
 * Date: 2019/12/2
 * Description:外来接口 后期做成在线配置
 */

return [
    //会员系统地址
    'memberApi' => [
        'maxDay' => 45,//最大查询天数
        'secret' => env('MEMBER_SECRET', 'agent_system'),
        'add' => env('MEMBER_API', '') . '/Agent_member/create',
        'update' => env('MEMBER_API', '') . '/Agent_member/update',
        'list' => env('MEMBER_API', '') . '/Agent_member/list',
        'detail' => env('MEMBER_API', '') . '/Agent_member/detail',
        'memberStatistics' => env('MEMBER_API', '') . '/Agent_member/statistics',
        'memberInfoByName' => env('MEMBER_API', '') . '/Agent_member/infoByName',
        'memberTransfer' => env('MEMBER_API', '') . '/Agent_member/transfer',
        'memberDayStatistics' => env('MEMBER_API', '') . '/Agent_member/dayStatistics',
        'memberCommissionStatistics' => env('MEMBER_API', '') . '/Agent_member/commissionStatistics',
        'noticeList' => env('MEMBER_API', '') . '/Agent_notice/list',
        'noticeCount' => env('MEMBER_API', '') . '/Agent_notice/count',
    ],
    'agentBk' => env('AGENT_BK', '')
];
