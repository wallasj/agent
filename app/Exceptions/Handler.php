<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Exception $e
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof AuthorizationException || $e instanceof AuthenticationException) {
            return $this->_changeFormat($e, '权限校验失败', '403');
        } else if ($e instanceof ValidationException) {
            return $this->_changeFormat($e, '参数错误', '400');
        } else if ($e instanceof NotFoundHttpException) {
            return $this->_changeFormat($e, '未找到该请求', '404');
        } else if (!$request->expectsJson()) {
            return $this->_changeFormat($e, '请求方式错误', '400');
        }

        return parent::render($request, $e);
    }

    //修改返回类型
    private function _changeFormat(Exception $e, string $message, string $code)
    {
        $erros = '';
        if (method_exists($e, 'errors')) {
            $erros = $e->errors();
        }
        return response()->json([
            'code' => $code,
            'message' => $message,
            'data' => $erros,
        ]);
    }
}
