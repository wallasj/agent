<?php

namespace App\Model;

use Baum\Node;

class Agent extends Node
{
    public $table = 'agent';

    /**
     * The attributes that are mass assignable.
     * 允许被写入的值
     * @var array
     */
    protected $fillable = [
        'commissionId',
        'agentNo',
        'agentName',
        'password',
        'payword',
        'realName',
        'level',
        'wallet',
        'realNameValid',
        'phone',
        'phoneValid',
        'email',
        'emailValid',
        'registerIp',
        'registerSource',
        'inviteCode',
        'status',
        'apiToken',
        'parent_id',
        'left',
        'right',
        'depth',
        'lastLoginIp',
        'lastLoginAt',
        'lastLoginSource'
    ];

    /**
     * The attributes that should be hidden for arrays.
     * 查询后隐藏的值
     * @var array
     */
    protected $hidden = [
        'payword',
        'password',
        'apiToken',
        'deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     * 查询后转换类型
     * @var array
     */
    protected $casts = [
    ];

    //获取代理信息
    public function agentInfo()
    {
        return $this->hasOne('App\Model\AgentInfo', 'agentId', 'id');
    }

    //获取代理佣金
    public function agentComissionStatistics()
    {
        return $this->hasMany('App\Model\AgentCommissionStatistics', 'agentId', 'id');
    }

    //获取代理日报表
    public function agentDayReport()
    {
        return $this->hasMany('App\Model\AgentDayReport', 'agentId', 'id');
    }

}
