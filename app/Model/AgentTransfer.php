<?php

namespace App\Model;

class AgentTransfer extends Base
{
    public $table = 'agent_transfer';

    /**
     * The attributes that are mass assignable.
     * 允许被写入的值
     * @var array
     */
    protected $fillable = [
        'agentId',
        'beforePpid',
        'afterPid',
        'status',
        'money',
        'beforeAgentMoney',
        'afterAgentMoney',
        'beforeMemberMoney',
        'afterMemberMoney'
    ];

    /**
     * The attributes that should be hidden for arrays.
     * 查询后隐藏的值
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be cast to native types.
     * 查询后转换类型
     * @var array
     */
    protected $casts = [
    ];

}
