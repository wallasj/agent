<?php

namespace App\Model;

class AgentDayReport extends Base
{
    public $table = 'agent_day_report';

    /**
     * The attributes that are mass assignable.
     * 允许被写入的值
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     * 查询后隐藏的值
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be cast to native types.
     * 查询后转换类型
     * @var array
     */
    protected $casts = [
    ];

}
