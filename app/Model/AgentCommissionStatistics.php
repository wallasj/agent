<?php

namespace App\Model;

class AgentCommissionStatistics extends Base
{
    public $table = 'agent_commission_statistics';

    /**
     * The attributes that are mass assignable.
     * 允许被写入的值
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     * 查询后隐藏的值
     * @var array
     */
    protected $hidden = [
        'auditTime',
        'auditIp',
        'adminId',
        'adminName'
    ];

    /**
     * The attributes that should be cast to native types.
     * 查询后转换类型
     * @var array
     */
    protected $casts = [
    ];

}
