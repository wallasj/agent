<?php

namespace App\Model;

class Letter extends Base
{
    public $table = 'agent_letter';

    /**
     * The attributes that are mass assignable.
     * 允许被写入的值
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'content',
        'type',
        'agentId'
    ];

    /**
     * The attributes that should be hidden for arrays.
     * 查询后隐藏的值
     * @var array
     */
    protected $hidden = [
        'id',
        'title',
        'content',
        'created_at'
    ];

    /**
     * The attributes that should be cast to native types.
     * 查询后转换类型
     * @var array
     */
    protected $casts = [
    ];

}
