<?php

namespace App\Model;

class Bank extends Base
{
    public $table = 'bank';

    /**
     * The attributes that are mass assignable.
     * 允许被写入的值
     * @var array
     */
    protected $fillable = [
        'id',
        'bankName',
        'bankCode',
        'bankImg',
        'status',
        'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     * 查询后隐藏的值
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be cast to native types.
     * 查询后转换类型
     * @var array
     */
    protected $casts = [
    ];

}
