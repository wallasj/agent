<?php

namespace App\Model;

class AgentBank extends Base
{
    public $table = 'agent_bank';

    /**
     * The attributes that are mass assignable.
     * 允许被写入的值
     * @var array
     */
    protected $fillable = [
        'agentId',
        'bankId',
        'isMain',
        'bankCode',
        'bankName',
        'bankAccount',
        'bankUserName',
        'bankBranchName',
        'status',
        'province',
        'city',
        'area',
        'bankCount'
    ];

    /**
     * The attributes that should be hidden for arrays.
     * 查询后隐藏的值
     * @var array
     */
    protected $hidden = [
        'agentId'
    ];

    /**
     * The attributes that should be cast to native types.
     * 查询后转换类型
     * @var array
     */
    protected $casts = [
    ];

    public function bankinfo()
    {
        return $this->hasOne('App\Model\Bank', 'id', 'bankId');
    }

}
