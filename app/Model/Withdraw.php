<?php

namespace App\Model;

class Withdraw extends Base
{
    public $table = 'agent_withdraw';

    const status = [
        -1 => '未通过',
         0 => '提交中',
         1 => '待审批',
         2 => '已通过'
    ];

    const types = [
        1 => '自发提款' ,
        2 => '财务出款'
    ];

    public static $_validRule = [
        'money' => 'required|numeric',
        'sources' => 'required',
        'bankId' => 'required|numeric',
        'payword' => 'required'
    ];

    public static $_messages = [
        'money.required' => '取款金额',
        'money.numeric' => '取款金额为数字',
        'sources.required' => '请填写来源',
        'bankId.required' => '请选择取款银行卡',
        'bankId.numeric' => '取款银行ID错误',
        'payword.required' => '取款密码'
    ];

    /**
     * The attributes that are mass assignable.
     * 允许被写入的值
     * @var array
     */
    protected $fillable = [
        'agentId',
        'bankId',
        'bankName',
        'bankCode',
        'bankAccount',
        'bankUserName',
        'money',
        'beforeAmount',
        'afterAmount',
        'remark',
        'status',
        'types',
        'adminId',
        'adminName',
        'auditTime',
        'sources',
        'ip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     * 查询后隐藏的值
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be cast to native types.
     * 查询后转换类型
     * @var array
     */
    protected $casts = [
    ];

}
