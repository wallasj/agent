<?php
/**
 *
 * User: wallas
 * Date: 2019/11/28
 * Description: 全局函数
 */

declare(strict_types=1);

/**
 * 小数点过滤
 * $amount 需要过滤的金额
 * $len 小数点保留长度
 * $withFormat 是否显示 1,000 科学计数格式
 */
if (!function_exists('amountFilter')) {
    function amountFilter($amount, int $len = 2, bool $withFormat = false) {
        // 非法数字,就不要返回了
        if (!is_numeric($amount)) {
            return intval($amount);
        }

        //如果为 0 直接返回
        if ($amount == 0) {
            return '0.00';
        }

        //返回整数
        if ($len === 0) {
            return number_format(intval($amount));
        }

        //如果是整数
        if (is_int($amount)) {
            if ($withFormat) {
                $amount = number_format($amount) . str_pad('.', $len, '0');
            } else {
                $amount = $amount . str_pad('.', $len, '0');
            }
        } else {
            //小数点分割
            $tem_arr = explode('.', (string)$amount);
            if (strlen($tem_arr[1]) <= $len) {
                if ($withFormat) {
                    $amount = number_format($tem_arr[0]) . '.' . str_pad($tem_arr[1], $len, '0');
                } else {
                    $amount = $tem_arr[0] . '.' . str_pad($tem_arr[1], $len, '0');
                }
            } else if (strlen($tem_arr[1]) > $len) {
                if ($withFormat) {
                    $amount = number_format($tem_arr[0]) . '.' . substr($tem_arr[1], 0, $len);
                } else {
                    $amount = $tem_arr[0] . '.' . substr($tem_arr[1], 0, $len);
                }
            }
        }
        return $amount;
    }
}

/**
 * 获取用户的来源
 * $source = [2=>'mobile_html5', 3=>'手机android APP', 4=>'手机IOS APP'];
 */
if (!function_exists('clientSourceType')) {
    function clientSourceType() {
        $isApp   = (!empty($_SERVER['HTTP_USER_AGENT']) && (substr($_SERVER['HTTP_USER_AGENT'], 0, 12) == 'great-winner') || strpos($_SERVER['HTTP_USER_AGENT'], 'great-winner') || (isset($_GET['isApp']) && $_GET['isApp']));
        $android = stripos($_SERVER['HTTP_USER_AGENT'], "Android") && stripos($_SERVER['HTTP_USER_AGENT'], "great-winner,Mobile");
        if ($isApp) {
            return $android ? 3 : 4;
        }
        return 2; // mobile_html5
    }
}

if (!function_exists('isHttps')) {
    function isHttps()
    {
        if ( ! empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off') {
            return true;
        } else if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
            return true;
        } else if ( ! empty($_SERVER['HTTP_FRONT_END_HTTPS']) && strtolower($_SERVER['HTTP_FRONT_END_HTTPS']) !== 'off') {
            return true;
        }
        return false;
    }
}

if (!function_exists('createOrderNo')) {
    function createOrderNo(string $bussinessName = "R")
    {
        $orderNo = date('Ymd') . substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
        return strtoupper($bussinessName . $orderNo);
    }
}

if (!function_exists('doSecret')) {
    function doSecret(string $str, string $method)
    {
        if (!empty($str)) {
            switch ($method) {
                case 'password':
                    return \Illuminate\Support\Facades\Hash::make($str . config('app.passwordPrefix'));
                    break;
                case 'username':
                    return sEncrypt($str, config('app.realNamePrefix'));
                    break;
                case 'email':
                    return sEncrypt($str, config('app.emailPrefix'));
                    break;
                case 'phone':
                    return sEncrypt($str, config('app.phonePrefix'));
                    break;
                default:
                    break;
            }
        }
        return '';
    }
}

if (!function_exists('unSecret')) {
    function unSecret($str, string $method)
    {
        if (!empty($str)) {
            try {
                switch ($method) {
                    case 'username':
                        return sDecrypt($str, config('app.realNamePrefix'));
                        break;
                    case 'email':
                        return sDecrypt($str, config('app.emailPrefix'));
                        break;
                    case 'phone':
                        return sDecrypt($str, config('app.phonePrefix'));
                        break;
                    default:
                        break;
                }
            } catch (Illuminate\Contracts\Encryption\DecryptException $e) {

            }
        }
        return '';
    }
}

//可逆加密
/*
*$data :需要加密解密的字符串
*$key :加密的钥匙(密匙);
*/
//加密
if (!function_exists('sEncrypt')) {
    function sEncrypt($data, $key)
    {
        $char = $str = '';
        $key = md5($key);
        $x = 0;
        $len = strlen($data);
        $l = strlen($key);
        for ($i = 0; $i < $len; $i++) {
            if ($x == $l) {
                $x = 0;
            }
            $char .= $key[$x];
            $x++;
        }
        for ($i = 0; $i < $len; $i++) {
            $str .= chr(ord($data[$i]) + (ord($char[$i])) % 256);
        }
        return base64_encode($str);
    }
}

//解密
if (!function_exists('sDecrypt')) {
    function sDecrypt($data, $key)
    {
        $char = $str = '';
        $key = md5($key);
        $x = 0;
        $data = base64_decode($data);
        $len = strlen($data);
        $l = strlen($key);
        for ($i = 0; $i < $len; $i++) {
            if ($x == $l) {
                $x = 0;
            }
            $char .= substr($key, $x, 1);
            $x++;
        }
        for ($i = 0; $i < $len; $i++) {
            if (ord(substr($data, $i, 1)) < ord(substr($char, $i, 1))) {
                $str .= chr((ord(substr($data, $i, 1)) + 256) - ord(substr($char, $i, 1)));
            } else {
                $str .= chr(ord(substr($data, $i, 1)) - ord(substr($char, $i, 1)));
            }
        }
        return $str;
    }
}

//判断头部
if (!function_exists('chcekHeader')) {
    function chcekHeader(string $type)
    {
        if (!empty($type)) {
            $type = strtolower($type);
            $value = request()->header($type);
            if (!empty($value))
            {
                switch ($type) {
                    case 'source':
                        return dictionary($type, strtoupper($value));
                    break;
                }
            }
        }
        return '';
    }
}

//redis
if (!function_exists('setRcache')) {
    function setRcache(string $name, $value, int $expires = 3600)
    {
        \Illuminate\Support\Facades\Redis::setex($name, $expires, $value);
    }
}

if (!function_exists('getRcache')) {
    function getRcache(string $name)
    {
        if (\Illuminate\Support\Facades\Redis::exists ($name))
        {
            return \Illuminate\Support\Facades\Redis::get($name);
        }
        return '';
    }
}

//log
if (!function_exists('logRecord')) {
    function logRecord($info, string $type = 'info')
    {
        $route = \Illuminate\Support\Facades\Route::current()->getActionName();
        if ($type === 'info') {
            \Illuminate\Support\Facades\Log::info($route . ' ' . $info);
            return ;
        } else if($type === 'warning') {
            \Illuminate\Support\Facades\Log::warning($route . ' ' . $info);
            return ;
        }
        \Illuminate\Support\Facades\Log::error($route . ' ' . $info);
    }
}

//字典
if (!function_exists('dictionary')) {
    function dictionary($name = '', $key = '')
    {
        $dictionary = config('dictionary');

        if ($name === '' && $key === 'all') {
            return $dictionary;
        }

        if ($name !== '' && $key === 'all') {
            if (strpos($name, ',') !== false) {
                $arr = explode(',', $name);
                $temp = [];
                foreach ($arr as $val) {
                    if (isset($dictionary[$val])) {
                        $temp[$val] = $dictionary[$val];
                    }
                }
                return $temp;
            }

            if (isset($dictionary[$name])) {
                return $dictionary[$name];
            }
        }

        if (isset($dictionary[$name])) {
            if (isset($dictionary[$name][$key])) {
                return $dictionary[$name][$key];
            } else if(in_array($key, $dictionary[$name])) {
                return $key;
            }
        }
        return '';
    }
}

/**
 * 分页计算
 * $count 总数
 * $nowPage 当前页
 * $limit 每页多少条
 */
if (!function_exists('getTotalPage'))
{
    function getTotalPage($count, int $nowPage, int $limit)
    {
        //默认15条
        if ($limit <= 0) $limit = config('app.limit');

        if ($nowPage == 0) $nowPage = config('app.pageSize');

        $nowPage = ($nowPage - 1) * $limit;

        $p =  (int)($count / $limit);
        $totalPage = $count % $limit == 0 ? $p : $p + 1;
        return [
            'total' => $totalPage,
            'page' => $nowPage,
            'limit' => $limit
        ];
    }
}

//当前时间
if (!function_exists('nowDate'))
{
    function nowDate(string $timeZone = 'Asia/Shanghai')
    {
        if ($timeZone === 'US') {
            $timeZone = 'US/Eastern';
        }
        return \Carbon\Carbon::now($timeZone)->format('Y-m-d H:i:s');
    }
}

if (!function_exists('timeRange'))
{
    function timeRange(string $type = 'today', string $timeZone = 'Asia/Shanghai')
    {
        $time = '';

        if ($timeZone === 'US') {
            $timeZone = 'US/Eastern';
        }

        $current = \Carbon\Carbon::now($timeZone);

        switch ($type) {
            case 'today':
                $time = $current->today();
            break;
            case 'yesterday':
                $time = $current->yesterday();
                break;
            case 'tomorrow':
                $time = $current->tomorrow();
                break;
            case 'weekStart':
                $time = $current->startOfWeek();
            break;
            case 'weekEnd':
                $time = $current->endOfWeek();
            break;
            case 'monthStart':
                $time = $current->startOfMonth();
            break;
            case 'lastMonthStart':
                $time = $current->subMonth()->firstOfMonth();
            break;
            case 'lastMonthEnd':
                $time = $current->subMonth()->endOfMonth();
            break;
            case 'yearStart':
                $time = $current->startOfYear();
            break;
        }

        return !empty($time) ? $time->format('Y-m-d H:i:s') : '';
    }
}

/**
 * 时间处理
 * $beginTime 开始时间
 * $endTime 结束时间
 * $maxDay  开始时间与当前时间的最大时间差
 */
if (!function_exists('timeRangeSql'))
{
    function timeRangeSql($beginTime, $endTime = '', $maxDay = 0)
    {
        //判断开始时间格式
        if (!empty($beginTime) && strtotime($beginTime) > 0) {
            //判断结束时间格式
            if (!empty($endTime) && !strtotime($endTime)) {
                return [];
            }

            if (strtotime($beginTime) > time()) {
                $beginTime = date('Y-m-d 00:00:00');
            }

            //判断开始时间是否符合时间差
            if (intval($maxDay) > 0) {
                //从开始时间往后算N天，如果小于当前时间，则表示超过了允许的范围，设置时间为从当天开始计算 前N天
                $current = \Carbon\Carbon::now();
                $begin = \Carbon\Carbon::parse($beginTime);
                $dt = $begin->diffInDays($current);
                if ($dt > $maxDay) {
                    $beginTime = $current->subDays($maxDay);
                }
            }

            if ($beginTime == $endTime || empty($endTime) || strtotime($endTime) < strtotime($beginTime)) {
                $endTime = nowDate();
            }

            //TODO 增加搜索逻辑
            return [
                'beginTime' => date('Y-m-d 00:00:00', strtotime($beginTime)),
                'endTime' => $endTime,
            ];
        }
        return [];
    }
}

if (!function_exists('isDate'))
{
    function isDate( $dateString ) {
        if (!strtotime($dateString)) return false;
        return strtotime( date('Y-m-d', strtotime($dateString)) ) === strtotime( $dateString );
        /*date函数会给月和日补零，所以最终用unix时间戳来校验*/
    }
}
