<?php

namespace App\Console\Commands;

use App\Http\Controllers\Api\Member\MemberController;
use App\Model\Agent;
use Illuminate\Console\Command;

class agentDayReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:dayReport {day? : (day重跑时间，eg.2020-01整月 2020-01-02当天)} {method? : (reset重跑脚本)} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'about agent commission day report';

    protected $oday = '';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //TODO 获取代理列表
        $data = [];
        $day = $this->argument('day') ?? 'today';
        $method = $this->argument('method') ?? 'normal';
        $list = Agent::where('status', 1)->orderBy('id', 'asc')->get();
        if ($method == 'reset' && isDate($day)) {
            //TODO 判断日期格式是否正确，且是属于日还是月
            $explode = explode('-', $day);
            //当日
            if (count($explode) == 3) {
                $this->_task($data, $list, $day . ' 00:00:00', $day . ' 23:59:59');
            } else {
                $now = date('Y-m-d 23:59:59');
                $maxDay = date('t', strtotime($day));
                for ($i = 1; $i <= $maxDay; ++$i) {
                    $beginDay = $day . '-' . str_pad($i, 2, '0', STR_PAD_LEFT) . ' 00:00:00';
                    $endDay = $day . '-' . str_pad($i, 2, '0', STR_PAD_LEFT) . ' 23:59:59';
                    if (strtotime($endDay) >= strtotime($now)) {
                        break;
                    }
                    $this->_task($data, $list, $beginDay, $endDay);
                }
            }
        } else {
            $this->_task($data, $list, date("Y-m-d 00:00:00", strtotime("-1 day")), date("Y-m-d 23:59:59", strtotime("-1 day")));
        }
        if (count($data) > 0) {
            \App\Model\AgentDayReport::insert($data);
        }
        return ' done ' . count($data);
    }

    private function _task(&$data, $list, $btime, $etime)
    {
        $this->oday = date('Y-m-d 00:00:00', strtotime($btime . ' +1 day'));
        foreach ($list as $agent) {
            $searchId = [];
            $count = \App\Model\AgentDayReport::where('agentId', $agent->id)->where('created_at', $this->oday)->count();
            if ($count <= 0) {
                $searchId[] = $agent->id;
                $children = Agent::where('left', '>', $agent->left)
                    ->where('right', '<', $agent->right)
                    ->where('status', 1)
                    ->get();
                if (!$children->isEmpty()) {
                    foreach ($children as $cagent) {
                        $searchId[] = $cagent->id;
                    }
                }
                $result = $this->_insertData($agent, $searchId, $btime, $etime);
                if (!empty($result)) {
                    $data[] = $result;
                }
            }
        }
    }

    private function _insertData($agent, $searchId, $btime, $etime)
    {
        $gdate = [];
        try {
            $member = new MemberController();
            $gdate = $member->getDayStatistics(implode(',', $searchId), [
                'beginTime' => $btime,
                'endTime' => $etime
            ]);
            if ($gdate) {
                $gdate = json_decode($gdate);
                $gdate = (array)$gdate->data;
            }
        } catch(\Exception $e) {
        }

        return [
            'agentId' => $agent->id,
            'agentPid' => $agent->parent_id,
            'agentNo' => $agent->agentNo,
            'agentName' => $agent->agentName,
            'registerCount' => $gdate['registerCount'] ?? 0,
            'totalAccount' => $gdate['registerCount'] ?? 0,
            'totalEarnings' => 0,
            'totalSingle' => $gdate['winningsAmount'] ?? 0,
            'firstDeposit' => $gdate['firstDepositCount'] ?? 0,
            'secondDeposit' => $gdate['secondDeposit'] ?? 0,
            'thirdDeposit' => $gdate['thirdDeposit'] ?? 0,
            'depositAmount' => $gdate['depositAmount'] ?? 0,
            'withdrawAmount' => $gdate['withdrawAmount'] ?? 0,
            'dwRate' => $gdate['dwRate'] ?? 0,
            'promoteAmount' => $gdate['promoteAmount'] ?? 0,
            'betAmount' => $gdate['betAmount'] ?? 0,
            'betValidAmount' => $gdate['betValidAmount'] ?? 0,
            'betValidPerson' => $gdate['betValidPerson'] ?? 0,
            'payoutAmount' => $gdate['payoutAmount'] ?? 0,
            'winningsAmount' => $gdate['winningsAmount'] ?? 0,
            'profitAmount' => $gdate['profitAmount'] ?? 0,
            'created_at' => $this->oday,
        ];
    }
}
