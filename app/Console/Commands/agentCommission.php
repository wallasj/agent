<?php

namespace App\Console\Commands;

use App\Http\Controllers\Api\Member\MemberController;
use App\Model\Agent;
use App\Model\AgentCommissionRule;
use Illuminate\Console\Command;

class agentCommission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commission:statistics {day? : (day重跑时间，eg.2020-01-02当天)} {method? : (reset重跑脚本)} ';

    protected $oday = '';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'about agent commission statistics';

    protected $timeRange = [];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $day = $this->argument('day') ?? 'today';
        $method = $this->argument('method') ?? 'normal';
        $now = time();
        if ($method == 'reset' && isDate($day)) {
            $explode = explode('-', $day);
            //当日
            if (count($explode) == 3) {
                $now = strtotime($day . ' 00:00:00');
            }
        }

        $sub = -6 - (date("w", $now) == 0 ? 7 : date("w", $now));
        $this->timeRange[0] = [
            date('Y-m-d 00:00:00', strtotime('-1 day', $now)),
            date('Y-m-d 23:59:59', strtotime('-1 day', $now)),
        ];
        $this->timeRange[1] = [
            date("Y-m-d 00:00:00", strtotime($sub . ' day', $now)),
            date('Y-m-d 23:59:59', strtotime('-1 sunday', $now)),
        ];
        $this->timeRange[2] = [
            date('Y-m-d 00:00:00', strtotime('-1 month', strtotime(date('Y-m', $now) . '-01 00:00:00'))),
            date('Y-m-d 23:59:59', strtotime(date('Y-m', $now) . '-01 00:00:00') - 86400),
        ];

        //TODO 获取代理列表
        $data = [];
        $list = Agent::where([
                'status' => 1,
                'parent_id' => 0
            ])->get();

        //TODO 获取默认佣金方案ID
        $ctype = \App\Model\AgentCommission::select('id')
                ->where('commissionStatus' , 1)
                ->where('isDefault', 1)
                ->first();

        //TODO 获取所有佣金方案
        $agentCommission = \App\Model\AgentCommission::where('commissionStatus', 1)->get()->toArray();
        if (empty($agentCommission)) {
            logRecord('脚本 - 没有佣金方案');
            return [];
        }

        if (!empty($ctype)) {
            foreach ($list as $agent) {
                $searchId = [];
                $commission = [];

                //TODO 判断用户是否有佣金方案
                $commissionId = $agent->commissionId ? $agent->commissionId : $ctype->id;

                //TODO 判断当前用户的佣金方案
                foreach ($agentCommission as $val) {
                    if ($val['id'] == $commissionId) {
                        $commission = $val;
                    }
                }

                //没有佣金方案，直接跳过
                if (empty($commission)) break;

                //TODO 获取代理佣金规则
                $rules = AgentCommissionRule::where('commissionId', $commissionId)->get();
                if (empty($rules)) {
                    logRecord('脚本 - 没有佣金配置, 佣金ID ' . $commissionId);
                    break;
                }

                //佣金方案状态 $commission['commissionType']
                if (isset($this->timeRange[$commission['commissionTime']])) {
                    //判断是否已经生成了佣金方案
                    $count = \App\Model\AgentCommissionStatistics::where('agentId', $agent->id)
                        ->where('endTime', $this->timeRange[$commission['commissionTime']][1])
                        ->count();
                    if ($count <= 0) {
                        $searchId[] = $agent->id;

                        $children = Agent::where('left', '>', $agent->left)
                            ->where('right', '<', $agent->right)
                            ->where('status', 1)
                            ->get();
                        if (!$children->isEmpty()) {
                            foreach ($children as $cagent) {
                                $searchId[] = $cagent->id;
                            }
                        }

                        $this->oday = date('Y-m-d 00:00:00', strtotime($this->timeRange[$commission['commissionTime']][1] . ' +1 day'));
                        $creturn = $this->_insertData($agent, $searchId, $this->timeRange[$commission['commissionTime']], $rules, $commission);
                        if (!empty($creturn)) {
                            $data[] = $creturn;
                        }
                    }
                } else {
                    break;
                }
            }

            if (count($data) > 0) {
                \App\Model\AgentCommissionStatistics::insert($data);
            }
        }
        return ' done ' . count($data);
    }

    private function _insertData($agent, $searchId, $time, $rules, $agentCommission)
    {
        if ($time) {
            $gdate = [];
            try {
                $member = new MemberController();
                $gdate = $member->getCommissionStatistics(implode(',', $searchId), [
                    'beginTime' => $time[0],
                    'endTime' => $time[1]
                ]);
                if ($gdate) {
                    $gdate = json_decode($gdate);
                    $gdate = (array)$gdate->data;
                }
            } catch(\Exception $e) {
                echo $e->getMessage();
                return [];
            }

            $commissionAmount = 0;
            $rate = 0;
            if ($gdate) {
                //判断佣金是否符合规范
                if (intval($gdate['profitAmount']) > 0 && $gdate['activeMember'] >= $agentCommission['validMember'] && $gdate['validAmount'] >= $agentCommission['validAmount']) {
                    //判断佣金区间
                    foreach ($rules as $val) {
                        if ($val->minAmount <= $gdate['profitAmount'] && $val->maxAmount > $gdate['profitAmount']) {
                            $rate = $val->rate > 0 ? $val->rate / 100 : 0;
                            $commissionAmount = $gdate['profitAmount'] * $rate;
                            break;
                        }
                    }
                }
            }

            //获取上期佣金
            $lastCommission = \App\Model\AgentCommissionStatistics::where('agentId', $agent->id)
                ->where('status', 1)
                ->where('created_at', '<=', $this->oday)
                ->orderBy('created_at', 'desc')
                ->first();

            return [
                'agentId' => $agent->id,
                'balance' => $agent->wallet,
                'agentName' => $agent->agentName,
                'proposalNo' => createOrderNo('CM'),
                'commissionTime' => date('Ymd'),
                'commissionType' => $agentCommission['commissionType'],
                'commissionName' => $agentCommission['commissionName'],
                'lastSurplus' => $lastCommission ? amountFilter($lastCommission->commissionAmount + $lastCommission->lastSurplus) : 0,
                'commissionAmount' => $commissionAmount,
                'status' => 0,
                'activeMember' => $gdate['activeMember'] ?? 0,
                'payoutAmount' => $gdate['payoutAmount'] ?? 0,
                'promoteAmout' => $gdate['promoteAmout'] ?? 0,
                'profitAmount' => $gdate['profitAmount'] ?? 0,
                'actualCommission' => 0,
                'beginTime' => $time[0],
                'endTime' => $time[1],
                'created_at' => $this->oday,
                'adminId' => 1,
                'adminName' => 'admin',
                'validAmount' => $gdate['validAmount'],
                'rate' => $rate
            ];
        }

        return [];
    }

}
