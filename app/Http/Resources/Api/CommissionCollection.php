<?php

namespace App\Http\Resources\Api;

use App\Http\Resources\BaseCollection;
use Carbon\Carbon;

class CommissionCollection extends BaseCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     * 佣金报表 - 列表
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'agentId' => $this->agentId,
            'proposalNo' => $this->proposalNo,
            'commissionTime' => $this->commissionTime,
            'commissionName' => $this->commissionName,
            //本期佣金 = (公司总盈利-线下运营成本-银行卡手续费-其他成本) * 25%
            'commissionAmount' => amountFilter($this->commissionAmount),
            'status' => $this->status,
            'statusText' => dictionary('commissionType', $this->status),
            'beginTime' => $this->beginTime ? Carbon::parse($this->beginTime)->format('Y-m-d H:i:s') : '',
            'endTime' => $this->endTime ? Carbon::parse($this->endTime)->format('Y-m-d H:i:s') : ''
        ];
    }

    //佣金报表 - 子代理报表
    public function agentReport()
    {
        return [
            'id' => $this->id,
            'agentName' => $this->agentName,
            'depositAmount' => amountFilter($this->depositAmount),
            'withdrawAmount' => amountFilter($this->withdrawAmount),
            'promoteAmount' => amountFilter($this->promoteAmount),
            'betAmount' => amountFilter($this->betAmount),
            'betValidAmount' => amountFilter($this->betValidAmount),
            'payoutAmountdaili' => amountFilter($this->payoutAmount)
        ];
    }

    //佣金报表 - 代理日报表
    public function dailyReport()
    {
        return [
            'id' => $this->id,
            'agentName' => $this->agentName,
            'registerCount' => intval($this->registerCount),
            'totalAccount' => intval($this->totalAccount),
            'totalEarnings' => amountFilter($this->totalEarnings),
            'totalSingle' => intval($this->totalSingle),
            'depositAmount' => amountFilter($this->depositAmount),
            'withdrawAmount' => amountFilter($this->withdrawAmount),
            'dwRate' => floatval($this->dwRate),
            'promoteAmount' => amountFilter($this->promoteAmount),
            'betAmount' => amountFilter($this->betAmount),
            'betValidAmount' => amountFilter($this->betValidAmount),
            'payoutAmount' => amountFilter($this->payoutAmount)
        ];
    }

    //佣金报表 - 输赢报表
    public function winOrLose()
    {
        return [
            'id' => $this->id,
            'agentName' => $this->agentName,
            'depositAmount' => amountFilter($this->depositAmount),
            'withdrawAmount' => amountFilter($this->withdrawAmount),
            'totalSingle' => $this->totalSingle,
            'betAmount' => amountFilter($this->betAmount),
            'betValidAmount' => amountFilter($this->betValidAmount),
            'promoteAmount' => amountFilter($this->promoteAmount),
            'payoutAmount' => amountFilter($this->payoutAmount),
            'winningsAmount' => amountFilter($this->winningsAmount)
        ];
    }

    //佣金报表 - 佣金明细
    public function details()
    {
        return [
            'id' => $this->id,
            'wallet' => amountFilter($this->balance),
            'beginTime' => $this->beginTime ? Carbon::parse($this->beginTime)->format('Y-m-d') : '',
            'endTime' => $this->endTime ? Carbon::parse($this->endTime)->format('Y-m-d') : '',
            'commissionName' => $this->commissionName,
            //本期佣金 = (公司总盈利-线下运营成本-银行卡手续费-其他成本) * 25%
            'commissionAmount' => (amountFilter($this->profitAmount) - amountFilter($this->commissionAmount)),
            'status' => $this->status,
            'statusText' => dictionary('commissionType', $this->status),
            'lastSurplus' => amountFilter($this->lastSurplus),
            'actualCommission' => amountFilter($this->actualCommission),
            'payoutAmount' => amountFilter($this->payoutAmount),
            'promoteAmout' => amountFilter($this->promoteAmout),
            'profitAmount' => amountFilter($this->profitAmount),
            'activeCount' => $this->activeMember,
            'offlineCost' => $this->offlineCost,
            'bankRate' => $this->bankRate,
            'otherCost' => $this->otherCost,
            'remark' => $this->remark
        ];
    }

}
