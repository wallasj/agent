<?php

namespace App\Http\Resources\Api;

use App\Http\Resources\BaseCollection;
use App\Model\AgentCommissionStatistics;
use App\Model\AgentDayReport;
use Carbon\Carbon;
class AgentCollection extends BaseCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'agentNo' => $this->agentNo,
            'agentName' => $this->agentName,
            'wallet' => amountFilter($this->wallet),
            'realName' => unSecret($this->realName, 'username'),
            'phone' => unSecret($this->phone, 'phone'),
            'email' => unSecret($this->email, 'email'),
            'level' => $this->level,
            'status' => $this->status,
            'created_at' => $this->created_at ? Carbon::parse($this->created_at)->format('Y-m-d H:i:s') : '',
            'updated_at' => $this->updated_at ? Carbon::parse($this->updated_at)->format('Y-m-d H:i:s') : ''
        ];
    }

    public function details()
    {
        $agentCommission = AgentCommissionStatistics::on('mysql_slave')
                ->select('lastSurplus', 'payoutAmount', 'promoteAmout', 'profitAmount', 'activeMember', 'commissionAmount')
                ->where([
                    'agentId' => $this->id,
                    'status' => 1
                ])
                ->orderBy('created_at', 'DESC')
                ->first();

        //获取代理日报表
        $agentDayCommission = AgentDayReport::on('mysql_slave')->select('profitAmount')
                ->where('agentId', $this->id)
                ->whereBetween('created_at', [date('Y-m-d 00:00:00'), date('Y-m-d H:i:s')])
                ->first();

        return [
            'id' => $this->id,
            'agentNo' => $this->agentNo,
            'agentName' => $this->agentName,
            'wallet' => amountFilter($this->wallet),
            'realName' => unSecret($this->realName, 'username'),
            'phone' => unSecret($this->phone, 'phone'),
            'email' => unSecret($this->email, 'email'),
            'underCount' => $this->children()->count(),
            'registerCount' => 0,
            'loginCount' => 0,
            'firstDepositRate' => '0%',
            'lastCommission' => $agentCommission ? amountFilter($agentCommission->lastSurplus) : 0,
            'activeCount' => $agentCommission->activeMember ?? 0,
            'payoutAmount' => $agentCommission ? amountFilter($agentCommission->payoutAmount) : 0,
            'promoteAmount' => $agentCommission ? amountFilter($agentCommission->promoteAmout) : 0,
            'profitAmount' => $agentCommission ? amountFilter($agentCommission->profitAmount) : 0,//取代理日报表
            'profitDayAmount' => $agentDayCommission ? amountFilter($agentDayCommission->profitAmount) : 0,//取代理日报表
            //本期佣金 = (公司总盈利-线下运营成本-银行卡手续费-其他成本) * 25%
            'nowCommission' => $agentCommission ? amountFilter($agentCommission->commissionAmount) : 0
        ];
    }
}
