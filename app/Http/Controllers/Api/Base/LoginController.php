<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\Base;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Agent\AgentLoginRequest;
use App\Model\AgentConfig;
use App\Model\AgentInfo;
use App\Model\LoginLog;
use App\Model\Agent;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @group 1.基础模块
 * Class LoginController
 * @package App\Http\Controllers\Api\Base
 * https://itfun.tv/news/164
 */
class LoginController extends BaseController
{

    /**
     * 1.1 代理登录
     *
     * @bodyParam agentName required string 代理名.
     * @bodyParam password required string 代理密码.
     *
     * @response {
            "message": "处理成功",
            "code": 0,
            "data": {
                "token": ""
            }
        }
     * @param AgentLoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(AgentLoginRequest $request)
    {
        $agentName = $request->agentName;

        $password = $request->password;

        $source = chcekHeader('source');

        if (!$source) {
            return $this->errorResponse('请传入正确的请求来源');
        }

        //TODO 验证码

        //获取用户信息
        $agent = Agent::on('mysql')->where('agentName', $agentName)->first();

        if (!empty($agent)) {
            if ($agent->status <= 0) {
                return $this->errorResponse('您的账号状态为【' . dictionary('status', $agent->status) . '】，请联系管理员');
            }
        }

        //验证参数
        $credentials = [
            'agentName' => $agentName,
            'password' => $password . config('app.passwordPrefix'),
            'status' => 1
        ];

        if (!$token = auth('api')->attempt($credentials)) {
            logRecord("user password error {$agentName}");
            return $this->errorResponse('用户名或密码错误');
        }

        //清空TOKEN
        if (!empty($agent->apiToken)) {
            if (JWTAuth::check($agent->apiToken)) {
                JWTAuth::setToken($agent->apiToken)->invalidate();
            }
        }

        //修改TOKEN
        $ip = $request->getClientIp();
        $agent->apiToken = $token;
        $agent->lastLoginIp = $ip;
        $agent->lastLoginAt = date('Y-m-d H:i:s');
        $agent->lastLoginSource = $source;
        $agent->save();

        //TODO 登录日志
        LoginLog::on('mysql')->create([
            'agentId' => $agent->id,
            'agentNo' => $agent->agentNo,
            'agentName' => $agent->agentName,
            'loginIp' => $ip,
            'source' => $source,
            'token' => $token
        ]);

        return $this->successResponse([
            'token' => 'Bearer '.$token
        ]);
    }

    /**
     * 1.2 退出登录
     * @authenticated
     *
     * @response {
     *   "message": "处理成功",
     *   "code": 0,
     *   "data": []
     *   }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginOut()
    {
        try {
            JWTAuth::setToken(JWTAuth::getToken())->invalidate();
            auth()->logout();

            //TODO 登出日志
            return $this->successResponse([], '退出登录成功');
        } catch (\Exception $e) {
            logRecord($e->getMessage());
        }
        return $this->errorResponse();
    }

    /**
     * 1.3 刷新token令牌
     * @authenticated
     *
     * @response {
     *   "message": "处理成功",
     *   "code": 0,
     *   "data": {
     *           "token": ""
     *       }
     *   }
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        try {
            $token = auth('api')->refresh();
            JWTAuth::setToken(JWTAuth::getToken())->invalidate();
            //TODO 刷新日志
            return $this->successResponse([
                'token' => 'Bearer '.$token
            ]);
        } catch (\Exception $e) {
            //TODO 添加日志
            logRecord($e->getMessage());
        }
        return $this->errorResponse();
    }

    /**
     * 1.4 获取登陆者信息
     * @return \Illuminate\Http\JsonResponse
     */
    public function info()
    {
        $https = isHttps() ? 'https://' : 'http://';
        //TODO 判断是否存在推广链接
        $agentInfo = AgentInfo::select('generalizeLinkMain', 'generalizeLinkSecond', 'generalizeLinkThird')
            ->where('agentId', $this->_auth->id)
            ->first();
        if ($agentInfo) {
            $url = $agentInfo->generalizeLinkMain ? $agentInfo->generalizeLinkMain : (
                    $agentInfo->generalizeLinkSecond ? $agentInfo->generalizeLinkSecond : (
                    $agentInfo->generalizeLinkThird ? $agentInfo->generalizeLinkThird : ''
                    ));
        }

        if (empty($url)) {
            //TODO 判断是否有默认的推广链接
            $defaultUrl = AgentConfig::select('defaultUrl')->where('id', 1)->first();
            if ($defaultUrl) {
                $url = $defaultUrl->defaultUrl . $this->_auth->inviteCode;
            }
        }

        if (empty($url)) {
            $url = $https . $_SERVER["HTTP_HOST"] . '?Palcode=' . $this->_auth->inviteCode;
        }

        $agent = Agent::where('id', $this->_auth->id)->first();

        $this->_auth->setPayword = $agent->payword ? 1 : 0;
        $this->_auth->inviteUrl = $url;
        return $this->successResponse($this->_auth);
    }
}
