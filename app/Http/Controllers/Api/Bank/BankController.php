<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\Bank;

use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\Api\AgentCollection;
use App\Model\Agent;
use App\Model\AgentBank;
use App\Model\Bank;
use Illuminate\Http\Request;

/**
 * @group 6.银行卡
 * Class AgentController
 * @package App\Http\Controllers\Api\Agent
 */
class BankController extends BaseController
{

    /**
     * 6.1 银行卡列表\
     *
     * @queryParam page int 当前页数. Example: 1
     * @queryParam pageSize int 每页数量. Example: 15
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function bankList(Request $request)
    {
        $query = Bank::on('mysql_slave')->getQuery();

        $query->where('status', 1);

        $query->where('type', 2);

        //TODO 获取总数
        $agentCount = $query->count();

        if ($agentCount > 0) {
            //分页参数
            $nowPage = $request->get('page', 1);

            $pageSize = $request->get('pageSize', 15);

            //TODO 分页
            $pagenation = getTotalPage($agentCount, (int)$nowPage, (int)$pageSize);

            $list = $query
                ->select('id', 'bankName', 'bankCode', 'bankImg')
                ->offset($pagenation['page'])
                ->limit($pagenation['limit'])
                ->get()
                ->toArray();

            foreach ($list as $key => $val) {
                $list[$key]->bankImg = config('api.agentBk') . $val->bankImg;
            }

            return $this->successResponse([
                'list' => $list,
                'count' => $agentCount,
                'totalPage' => $pagenation['total']
            ]);
        }
        return $this->successNullResponse();
    }

    /**
     * 6.2 查询当前用户银行卡
     * @authenticated
     *
     * @response{
     *       "message": "处理成功",
     *       "code": 0,
     *       "data": [
     *          "id" : "id",
     *          "agentId" : "代理ID",
     *          "bankImg" : "银行图片",
     *          "bankCode" : "银行编号",
     *          "bankName" : "银行名称",
     *          "bankAccount" : "银行卡号",
     *          "bankUserName" : "开户人",
     *          "bankBranchName" : "支行地址"
     *       ]
     *   }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function agentBankList()
    {
        $agentId = $this->_auth->id;

        $count = AgentBank::where('agentId', $agentId)->where([
            'status' => 1
        ])->count();

        $data = [];

        if ($count > 0) {
            $agentBank = AgentBank::where('agentId', $agentId)
                ->where('status', 1)
                ->get();

            if (!$agentBank->isEmpty()) {
                foreach ($agentBank as $key => $val) {
                    $bankInfo = $val->bankinfo;
                    $data[] = [
                        'id' => $val->id,
                        'bankId' => $val->bankId,
                        'bankCode' => $val->bankCode,
                        'bankName' => $val->bankName,
                        'bankAccount' => $val->bankAccount,
                        'bankUserName' => $val->bankUserName,
                        'bankBranchName' => $val->bankBranchName,
                        'bankImg' => $bankInfo->bankImg ? config('api.agentBk') . $bankInfo->bankImg : ''
                    ];
                }
            }
        }

        return $this->successResponse([
            'list' => $data,
            'count' => $count
        ]);
    }

    /**
     * 6.3 查询当前用户银行卡信息
     * @authenticated
     *
     * @queryParam bankId int 银行卡ID.
     *
     * @response{
     *       "message": "处理成功",
     *       "code": 0,
     *       "data": [
     *          "id" : "id",
     *          "agentId" : "代理ID",
     *          "bankCode" : "银行编号",
     *          "bankName" : "银行名称",
     *          "bankAccount" : "银行卡号",
     *          "bankUserName" : "开户人",
     *          "bankBranchName" : "支行地址"
     *       ]
     *   }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function agentBankInfo(Request $request)
    {
        $agentId = $this->_auth->id;

        $bankId = $request->get('id', 0);

        if (intval($bankId) > 0) {
            $agentBank = AgentBank::where([
                    'bankId' => $bankId,
                    'agentId' => $agentId,
                    'status' => 1
                ])
                ->first();

            if ($agentBank) {
                $bankInfo = $agentBank->bankinfo;
                $data = [
                    'id' => $agentBank->id,
                    'agentId' => $agentBank->agentId,
                    'bankId' => $agentBank->bankId,
                    'bankCode' => $agentBank->bankCode,
                    'bankName' => $agentBank->bankName,
                    'bankAccount' => $agentBank->bankAccount,
                    'bankUserName' => $agentBank->bankUserName,
                    'bankBranchName' => $agentBank->bankBranchName,
                    'bankImg' => $bankInfo->bankImg ? config('api.agentBk') . $bankInfo->bankImg : ''
                ];
                return $this->successResponse($data);
            }

            return $this->errorResponse('未找到银行卡信息');
        }

        return $this->errorResponse('参数错误');
    }

    /**
     * 6.4 添加代理银行卡
     * @authenticated
     *
     * @bodyParam bankId int 银行ID.
     * @bodyParam bankAccount string 银行账号.
     * @bodyParam bankUserName string 开户人.
     * @bodyParam bankBranchName string 开户支行.
     *
     * @response{
     *       "message": "添加成功",
     *       "code": 0,
     *       "data": []
     *   }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addAgentBank(Request $request)
    {
        $agentId = $this->_auth->id;

        $bankId = $request->get('bankId', '');

        if(!empty($bankId)) {
            //TODO 检查银行卡是否存在
            $bank = Bank::where([
                'id' => intval($bankId),
                'status' => 1,
                'type' => 2
            ])->first();

            if ($bank) {

                $bankAccount = $request->get('bankAccount', '');

                $bankUserName = $request->get('bankUserName', '');

                $bankBranchName = $request->get('bankBranchName', '');

                if (empty($bankAccount) || empty($bankUserName) || empty($bankBranchName)) {
                    return $this->errorResponse('缺少参数');
                }

                if (!is_numeric($bankAccount)) {
                    return $this->errorResponse('银行卡号请输入数字');
                }

                //TODO 判断银行卡数量是否超过3张
                $count = AgentBank::where([
                    'agentId' => $agentId,
                    'status' => 1
                ])->count();

                if ($count >= 3) {
                    return $this->errorResponse('最多三张银行卡');
                }

                $agentBank = [
                    'agentId' => $agentId,
                    'isMain' => 0,
                    'bankId' => $bank->id,
                    'bankCode' => $bank->bankCode,
                    'bankName' => $bank->bankName,
                    'bankAccount' => $bankAccount,
                    'bankUserName' => $bankUserName,
                    'bankBranchName' => $bankBranchName,
                    'status' => 1,
                    'bankCount' => $count + 1
                ];

                $bank = AgentBank::create($agentBank);

                if ($bank) {
                    return $this->successResponse([], '银行卡创建成功');
                }

                return $this->errorResponse('银行卡创建失败');
            } else {
                return $this->errorResponse('未找到银行卡信息');
            }
        }

        return $this->errorResponse('缺少参数');
    }

    /**
     * 6.5 删除银行卡
     * @authenticated
     *
     * @bodyParam bankId int 银行ID.
     *
     * @response{
     *       "message": "添加成功",
     *       "code": 0,
     *       "data": []
     *   }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delAgentBank(Request $request)
    {
        $agentId = $this->_auth->id;

        $bankId = $request->get('bankId', '');

        if (intval($bankId) > 0) {
            $agentBank = AgentBank::where('id', $bankId)
                ->where('agentId', $agentId)
                ->where('status', 1)
                ->first();

            if ($agentBank) {
                $count = AgentBank::where('agentId', $agentId)
                    ->where('status', 1)
                    ->count();
                if ($count <= 1) {
                    return $this->errorResponse('银行卡至少保留一张');
                }

                $delete = $agentBank->delete();
                if ($delete) {
                    return $this->successResponse([], '银行卡删除成功');
                }
                return $this->errorResponse('银行卡删除失败');
            }

            return $this->errorResponse('未查询到银行卡信息');
        }

        return $this->errorResponse('缺少参数');
    }

}
