<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\Withdraw;

use App\Http\Controllers\Api\BaseController;
use App\Model\Agent;
use App\Model\AgentBank;
use App\Model\Withdraw;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

/**
 * @group 7.取款模块
 * Class MemberController
 * @package App\Http\Controllers\Api\Member
 */
class WithdrawController extends BaseController
{

    /**
     * 7.1 取款申请
     * @authenticated
     *
     * @bodyParam bankId int required 银行卡ID.
     * @bodyParam money float required 取款金额.
     * @bodyParam payword string required 取款密码.
     * @bodyParam sources string 来源. Example: pc/h5/ios/android
     *
     * @response {
     *   "message": "存款成功",
     *   "code": 0,
     *   "data": []
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        try {
            $money = $request->get('money', 0);

            if (floatval($money) > 0) {

                //判断取款银行是否存在
                $bankId = $request->get('bankId', 0);

                $payword = $request->get('payword', '');

                if (!$payword) {
                    return $this->errorResponse('请输入取款密码');
                }

                $agentId = $this->_auth->id;

                $paywords = Agent::select('payword')
                ->where([
                    'id' => $agentId,
                    'status' => 1
                ])->first();

                if (!$paywords) {
                    return $this->errorResponse('请设置取款密码');
                }

                if (!Hash::check($payword . config('app.passwordPrefix'), $paywords->payword)) {
                    logRecord("agentName {$this->_auth->agentName} 支付密码错误");
                    return $this->errorResponse('支付密码错误');
                }

                //查询代理银行卡信息
                $bank = AgentBank::select('id', 'bankCode', 'bankName', 'bankAccount', 'bankUserName')
                    ->where([
                        'bankId' => $bankId,
                        'agentId' => $agentId,
                        'status' => 1
                    ])
                    ->first();

                if (!$bank) {
                    return $this->errorResponse('您还没有银行卡，请先绑定');
                }

                $sources = $request->input('sources', '');

                $source = dictionary('source', strtoupper($sources));

                if (!$source) {
                    return $this->errorResponse('请传入正确的请求来源');
                }

                $agent = Agent::on('mysql')
                    ->select('wallet')
                    ->where('id', $agentId)
                    ->where('status', 1)
                    ->first();
                if ($agent) {
                    //TODO 判断当前余额是否小于取款金额
                    if (floatval($agent->wallet) < floatval($money)) {
                        return $this->errorResponse('您的余额不足');
                    }

                    $withdraw = [
                        'agentId' => $agentId,
                        'bankId' => $bankId,
                        'bankName' => $bank->bankName,
                        'bankCode' => $bank->bankCode,
                        'bankAccount' => $bank->bankAccount,
                        'bankUserName' => $bank->bankUserName,
                        'money' => floatval($money),
                        'beforeAmount' => $agent->wallet,
                        'afterAmount' => floatval($agent->wallet) - floatval($money),
                        'status' => 1,
                        'types' => 1,
                        'ip' => $request->getClientIp(),
                        'sources' => $source
                    ];
                    $create = Withdraw::on('mysql')->create($withdraw);
                    if ($create) {
                        return $this->successResponse([], '取款成功');
                    }
                } else {
                    return $this->errorResponse('未找到代理信息');
                }
            } else {
                return $this->errorResponse('请输入正确的取款金额');
            }
        } catch(\Exception $e) {
            Log::info('取款提交失败' . $e->getMessage());
        }

        return $this->errorResponse('缺少参数');
    }

    /**
     * 7.2 获取当前用户的取款记录
     * @authenticated
     *
     * @queryParam beginTime string 开始时间. Example: YYYY-MM-DD HH:ii:ss
     * @queryParam endTime string 结束时间. Example: YYYY-MM-DD HH:ii:ss
     * @queryParam status int 取款状态. Example: 取款状态 -1未通过 0提交中 1待审批 2已通过
     * @queryParam page int 当前页数. Example: 1
     * @queryParam pageSize int 每页数量. Example: 15
     *
     * @response {
     *   "message": "处理成功",
     *   "code": 0,
     *   "data": [
     *          "id" : "id",
     *          "agentId" : "agentId",
     *          "money" : "取款金额",
     *          "status" : "取款状态 -1未通过 0提交中 1待审批 2已通过",
     *          "types" : "取款类型 1自发 2后台",
     *          "remark" : "备注",
     *          "auditTime" : "审核时间",
     *          "created_at" : "创建时间"
     *   ]
     *}
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function withDrawLog(Request $request)
    {
        $agentId = $this->_auth->id;

        $query = Withdraw::on('mysql')->getQuery();

        $status = $request->get('status', '');

        if (is_numeric($status)) {
            $query->where('status', $status);
        }

        //TODO 判断查询时间 是否为当天
        $beginTime = $request->get('beginTime', '');

        if (empty($beginTime)) {
            $beginTime = date('Y-m-d 00:00:00');
        }

        $endTime = $request->get('endTime', '');

        if (empty($endTime)) {
            $endTime = date('Y-m-d H:i:s');
        }

        $timeRange = timeRangeSql($beginTime, $endTime, 45);

        $query->whereBetween('created_at', [$timeRange['beginTime'], $timeRange['endTime']]);

        $query->where('agentId', $agentId)->get();

        //TODO 获取总数
        $withdrawCount = $query->count();

        if ($withdrawCount > 0) {
            //分页参数
            $nowPage = $request->get('page', 1);

            $pageSize = $request->get('pageSize', 15);

            //TODO 分页
            $pagenation = getTotalPage($withdrawCount, (int)$nowPage, (int)$pageSize);

            $list = $query
                ->select('id', 'agentId', 'money', 'status', 'types', 'remark', 'auditTime', 'created_at')
                ->offset($pagenation['page'])
                ->limit($pagenation['limit'])
                ->get();

            $arr = [];
            foreach ($list as $val) {
                $val->money = amountFilter($val->money);
                $val->typeText = Withdraw::types[$val->types];
                $val->statusText = Withdraw::status[$val->status];
                $arr[] = $val;
            }

            return $this->successResponse([
                'list' => $list,
                'count' => $withdrawCount,
                'totalPage' => $pagenation['total']
            ]);
        }

        return $this->successNullResponse();
    }

}
