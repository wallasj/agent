<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\Agent;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Api\Member\MemberController;
use App\Http\Requests\Agent\AgentPwdRequest;
use App\Http\Requests\Agent\AgentTransferRequest;
use App\Http\Requests\Agent\AgentUpdateRequest;
use App\Http\Resources\Api\AgentCollection;
use App\Http\Requests\Agent\AgentRegisterRequest;
use App\Model\AgentCommission;
use App\Model\AgentInfo;
use App\Model\AgentTransferLog;
use Illuminate\Http\Request;
use App\Model\Agent;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/**
 * @group 2.代理模块
 * Class AgentController
 * @package App\Http\Controllers\Api\Agent
 */
class AgentController extends BaseController
{

    /**
     * 2.1 代理下线会员
     * @authenticated
     *
     * @queryParam status int 代理状态【0 待审核/1 审批通过】.
     * @queryParam page int 当前页数. Example: 1
     * @queryParam pageSize int 每页数量. Example: 15
     *
     * @apiResourceCollection App\Http\Resources\Api\AgentCollection
     * @apiResourceModel App\Model\Agent
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function agentList(Request $request)
    {
        $query = Agent::on('mysql_slave')->getQuery();

        $query->where('parent_id', $this->_auth->id);

        $query->where('left', '>', $this->_auth->left);

        $query->where('right', '<', $this->_auth->right);

        $status = $request->get('status', 1);

        if (is_numeric($status) && $status != 99) {
            $query->where('status', $status);
        }

        //TODO 获取总数
        $agentCount = $query->count();

        if ($agentCount > 0) {
            //分页参数
            $nowPage = $request->get('page', 1);

            $pageSize = $request->get('pageSize', 15);

            //TODO 分页
            $pagenation = getTotalPage($agentCount, (int)$nowPage, (int)$pageSize);

            $list = $query
                ->select('id', 'agentNo', 'agentName', 'realName', 'status', 'phone', 'level', 'wallet', 'email', 'created_at', 'updated_at')
                ->offset($pagenation['page'])
                ->limit($pagenation['limit'])
                ->get();

            if (!$list->isEmpty()) {
                $agent = AgentCollection::collection($list);
                $list = $agent->toArray($request);

                return $this->successResponse([
                    'list' => $list,
                    'count' => $agentCount,
                    'totalPage' => $pagenation['total']
                ]);
            }
        }
        return $this->successNullResponse();
    }

    /**
     * 2.2 获取代理邀请码
     *
     * @response{
     *       "message": "处理成功",
     *       "code": 0,
     *       "data": {
     *           "inviteCode": "A2019120548995052"
     *       }
     *   }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function inviteCode()
    {
        return $this->successResponse([
            'inviteCode' => $this->_createInviteCode()
        ]);
    }

    /**
     * 2.3 注册代理
     *
     * @bodyParam agentNo required string 代理编号.
     * @bodyParam agentName required string 代理名.
     * @bodyParam password required string 代理密码.
     * @bodyParam realName required string 真实姓名.
     * @bodyParam inviteCode string 【上级代理inviteCode， 登录状态下为当前代理】.
     * @bodyParam phone int 手机号码.
     * @bodyParam email string 邮箱.
     * @bodyParam qq string QQ.
     * @bodyParam wechat string 微信号.
     * @bodyParam birthDay date 生日:YYYY-MM-DD.
     * @bodyParam status date 代理状态【0待审核 / -1禁用】.
     *
     * @response{
     *       "message": "处理成功",
     *       "code": 0,
     *       "data": []
     *   }
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AgentRegisterRequest $request)
    {
        $data = $request->all();

        //TODO 单独判断手机号码
        $data['phone'] = doSecret($data['phone'], 'phone');
        $count = Agent::on('mysql')
            ->where('phone', $data['phone'])
            ->count();
        if ($count > 0) {
            return $this->errorResponse('注册失败，请联系客服');
        }

        /**
         * 传入的inviteCode - 后面使用
         * 如果用户已登陆，则使用登陆者的TOKEN
         */
        $inviteCode = $data['inviteCode'] ?? '';
        if ($this->_auth) {
            $inviteCode = $this->_auth->inviteCode;
        } else {
            //未登录情况下所有的注册账号均为禁用
            $data['status'] = 0;
        }

        //Todo 密码做处理 - 默认支付密码与密码相等
        $data['password'] = doSecret($data['password'], 'password');

        $data['payword'] = '';

            //Todo 真实姓名处理
        $data['realName'] = doSecret($data['realName'], 'username');

        //Todo 邮箱处理
        if (!empty($data['email'])) {
            $data['email'] = doSecret($data['email'], 'email');
        } else {
            $data['email'] = '';
        }

        //Todo 邀请码
        $data['inviteCode'] = $this->_createInviteCode();

        $source = chcekHeader('source');

        if (!$source) {
            return $this->errorResponse('请传入正确的请求来源');
        }

        //获取注册IP
        $data['registerIp'] = $request->getClientIp();

        $data['registerSource'] = $source;

        //获取默认佣金方案
        $commissionId = 0;
        $commission = AgentCommission::select('id')
        ->where([
            'isDefault' => 1,
            'commissionStatus' => 1
        ])->first();
        if($commission) {
            $commissionId = $commission->id;
        }
        $data['commissionId'] = $commissionId;

        /**
         * 代理未登陆时
         *  查询传入的inviteCode 是否存在
         * 代理已登陆，则直接取用户ID
         */
        $parentId = null;
        $p_commissionId = '';
        if ($this->_auth) {
            $parentId = $this->_auth->id;
            $p_commissionId = $this->_auth->commissionId;
        } else {
            $parent = Agent::on('mysql')
                ->select('id', 'commissionId')
                ->where('inviteCode', $inviteCode)
                ->where('status', 1)
                ->first();
            if ($parent) {
                $parentId = $parent->id;
                $p_commissionId = $parent->commissionId;
            }
        }

        if (!empty($p_commissionId)) {
            $data['commissionId'] = $p_commissionId;
        }

        try{
            DB::beginTransaction();
            do {
                $agent = Agent::on('mysql')->create($data);
                $agentId = $agent->id;
                if ($agentId > 0) {
                    /**
                     * 插入代理信息
                     * QQ 生日 微信
                     */
                    $qq = $request->get('qq', '');
                    $wechat = $request->get('wechat', '');
                    $birthDay = $request->get('birthDay', '');

                    $infoData = [
                        'agentId' => $agentId,
                        'qq' => $qq,
                        'wechat' => $wechat,
                    ];

                    if (!empty($birthDay)) {
                        $infoData['birthDay'] = $birthDay;
                    }

                    $agentInfo = AgentInfo::on('mysql')->create($infoData);
                    if (!$agentInfo) {
                        break;
                    }

                    //创建层级
                    $node = $agent->makeRoot();
                    $node->parent_id = $parentId;
                    $save = $node->save();
                    if(!$save) {
                        break;
                    }
                    DB::commit();
                    return $this->successResponse([],'代理注册成功');
                }
            } while(0);
            DB::rollBack();
        } catch(\Exception $e) {
            logRecord($e->getMessage());
            return $this->errorResponse('代理注册失败', [
                'info' => $e->getMessage()
            ]);
        }
        return $this->errorResponse('代理注册失败');
    }

    /**
     * 2.4 代理详情
     * @authenticated
     *
     * @queryParam id int 代理ID. Example: 1
     *
     * @response {
     *   "message": "处理成功",
     *   "code": 0,
     *   "data": {
     *           "id": "",
     *           "agentNo": "",
     *           "agentName": "",
     *           "wallet": "",
     *           "realName": "",
     *           "phone": "",
     *           "email": "",
     *           "underCount": "下线代理数",
     *           "underMemberCount": "下线会员数",
     *           "registerCount": "下线会员注册总数",
     *           "loginCount": "下线会员登录总数",
     *           "firstDepositRate": "首存比例",
     *           "lastCommission": "上期佣金",
     *           "activeCount": "活跃会员数",
     *           "payoutAmount": "总派彩",
     *           "promoteAmount": "总优惠金额",
     *           "profitAmount": "总盈利",
     *           "nowCommission": "本期佣金"
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $id = $request->get('id', 0);

        if (!is_numeric($id) || intval($id) <= 0) {
            //判断是否登陆
            if ($this->_auth) {
                $agent = Agent::on('mysql_slave')->where('id', $this->_auth->id)
                            ->first();
            }
        } else {
            $agent = Agent::on('mysql_slave')
                        ->where('id', $id)
                        ->where('parent_id', $this->_auth->id)
                        ->where('left', '>', $this->_auth->left)
                        ->where('right', '<', $this->_auth->right)
                        ->first();
        }

        //TODO 设置REDIS
        if (!empty($agent)) {
            //TODO 详情表关联
            $agent = new AgentCollection($agent);
            $details = $agent->details();
            if (isset($agent->agentInfo)) {
                $details['underCount'] = $agent->agentInfo->count();
            }
            //查询当前代理下线用户
            $gdate = [];
            try {
                $member = new MemberController();
                $gdate = $member->getMemberStatistics();
                if ($gdate) {
                    $gdate = json_decode($gdate);
                    $gdate = (array)$gdate->data;
                }
            } catch(\Exception $e) {
            }
            $details = array_merge($details, $gdate);
            return $this->successResponse($details);
        }

        return $this->errorResponse('未查询到该代理信息');
    }

    /**
     * 2.5 修改代理密码
     * @authenticated
     *
     * @bodyParam type required string 密码类型 ['password', 'payword'] 中的一种.
     * @bodyParam oldPassword required string 旧密码.
     * @bodyParam newPassword required string 新密码.
     *
     * @response {
     *   "message": "处理成功",
     *   "code": 0,
     *   "data": 0
     *   }
     * @param AgentPwdRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePwd(AgentPwdRequest $request)
    {
        $data = $request->all();
        $originalPwd = $this->_auth->getAuthPassword();

        if (!$this->_auth->setPayword && $data['type'] == 'payword') {

        } else {
            //TODO 判断密码是否正确
            if (!Hash::check($data['oldPassword'], $originalPwd)) {
                logRecord("agentName {$this->_auth->agentName} 原密码校验失败");
                return $this->errorResponse('原密码校验失败');
            }
        }

        //TODO 验证码

        //修改用户密码
        $data = [
            "{$data['type']}" => doSecret($data['newPassword'], 'password')
        ];
        $update = Agent::on('mysql')->where('id', $this->_auth->id)->update($data);

        //TODO 添加日志
        if ($update) {
            logRecord("agentName {$this->_auth->agentName} 成功修改了密码");
            return $this->successResponse();
        }
        return $this->errorResponse('密码修改失败');
    }

    /**
     * 2.6 验证代理信息是否存在
     *
     * @bodyParam type required string 验证类型 ['inviteCode', 'agentName', 'domain'] 中的一种.
     * @bodyParam value required string 待验证值.
     *
     * @response {
     *   "message": "处理成功",
     *   "code": 0,
     *   "data": {
     *      "id" : "代理ID",
     *      "agentName" : "代理名称"
     *    }
     *   }
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkAgentBy(Request $request)
    {
        $type = $request->get('type', 'inviteCode');
        if (!in_array($type, ['inviteCode', 'agentName', 'domain'])) {
            return $this->errorResponse('缺少验证类型');
        }

        $value = $request->get('value', '');
        if (empty($value)) {
            return $this->errorResponse('缺少验证数据');
        }

        $info = [];
        if ($type == 'domain') {
            $agentId = AgentInfo::on('mysql')
                ->select('agentId')
                ->where('generalizeLinkMain', $value)
                ->orWhere('generalizeLinkSecond', $value)
                ->orWhere('generalizeLinkThird', $value)
                ->first();
            if ($agentId) {
                $info = Agent::on('mysql')->select('id', 'agentName')
                    ->where('id', $agentId->agentId)->first();
            }
        } else {
            $info = Agent::on('mysql')->select('id', 'agentName')
                ->where($type, $value)->first();
        }

        return $this->successResponse($info);
    }

    /**
     * 2.7 代理钱包转账 - 用户/代理
     *
     * @bodyParam username string required 下线代理名称.
     * @bodyParam money number required 转账金额.
     * @bodyParam payword string required 支付密码.
     * @bodyParam type string 转账类型【member/agent】.
     *
     * @response {
     *   "message": "处理成功",
     *   "code": 0,
     *   "data": 0
     *   }
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function transferMoney(AgentTransferRequest $request)
    {
        //TODO 判断当前账号的钱包金额是否大于
        $data = $request->all();

        //TODO 判断支付密码
        $payword = $request->get('payword', '');

        if (empty($payword)) {
            return $this->errorResponse('请输入支付密码');
        }

        $agent = Agent::on('mysql')
            ->where('id', $this->_auth->id)
            ->where('status', 1)
            ->first();

        if ($agent) {
            if (empty($agent->payword)) {
                return $this->errorResponse('您还未设置支付密码，请设置支付密码');
            }

            if (!Hash::check($payword . config('app.passwordPrefix'), $agent->payword)) {
                logRecord("agentName {$this->_auth->agentName} 支付密码错误");
                return $this->errorResponse('支付密码错误');
            }
        } else {
            return $this->errorResponse('未查询到代理信息');
        }

        $transTo = $request->get('type', 'member');
        if ($transTo == 'member') {
            //查询当前用户是否为代理下线
            try {
                $member = new MemberController();
                $memberInfo = $member->getMmeberByName($data['username']);
                if ($memberInfo) {
                    $memberInfo = json_decode($memberInfo);

                    if ($memberInfo->code != 0) {
                        return $this->errorResponse($memberInfo->message);
                    }

                    $memberInfo = (array)$memberInfo->data;

                    $agent = Agent::on('mysql')->select('wallet')->where('id', $this->_auth->id)
                        ->first();

                    if (floatval($agent->wallet) < floatval($data['money'])) {
                        return $this->errorResponse('您的账户余额不足');
                    }

                    //TODO 开始执行转账操作
                    try {
                        do {
                            DB::beginTransaction();
                            //添加下线钱包
                            $result = $member->transferMoney(intval($memberInfo['info']->id), floatval($data['money']));
                            $result = json_decode($result);
                            if ($result->code == 0) {
                                logRecord("转账 agentName {$this->_auth->agentName} 成功修改了余额");
                                logRecord("转账 memberName {$data['username']} 成功修改了余额");

                                //TODO 扣除当前账号余额
                                $update = Agent::on('mysql')->find($this->_auth->id)->update([
                                    'wallet' => floatval($agent->wallet - $data['money'])
                                ]);
                                if (!$update) {
                                    //TODO 检查是否已经给用户加钱了，是的话需要回滚
                                    logRecord("转账 memberName {$data['username']} 转账异常", 'error');
                                    return $this->errorResponse('转账异常');
                                }

                                //TODO 添加转账记录
                                AgentTransferLog::on('mysql')->create([
                                    'agentId' => $this->_auth->id,
                                    'toAgentName' => $this->_auth->agentName,
                                    'toMemberId' => $memberInfo['info']->id,
                                    'toMemberName' => $memberInfo['info']->username,
                                    'money' => $data['money'],
                                    'beforeAgentMoney' => floatval($agent->wallet),
                                    'afterAgentMoney' => floatval($agent->wallet - $data['money']),
                                    'beforeMemberMoney' => floatval($memberInfo['info']->balance),
                                    'afterMemberMoney' => floatval($memberInfo['info']->balance + $data['money']),
                                ]);

                                DB::commit();
                                return $this->successResponse();
                            }
                        } while(0);
                        DB::rollBack();
                    } catch (\Exception $e) {
                        logRecord($e->getMessage());
                        return $this->errorResponse('代理转账失败', [
                            'info' => $e->getMessage()
                        ]);
                    }
                }
            } catch(\Exception $e) {
                logRecord("转账查询用户总数失败 member {$data['username']}");
            }

        } else if($transTo == 'agent') {
            $underAgent = Agent::on('mysql_slave')
                ->select('id', 'parent_id', 'wallet')
                ->where('agentName', $data['username'])
                ->first();

            if (empty($underAgent)) {
                return $this->errorResponse('未查询到代理信息');
            }

            if ($underAgent->parent_id != $this->_auth->id) {
                return $this->errorResponse('未查询到子代理信息');
            }

            $agent = Agent::on('mysql')->select('wallet')->where('id', $this->_auth->id)
                ->first();
            if (floatval($agent->wallet) < floatval($data['money'])) {
                return $this->errorResponse('您的账户余额不足');
            }

            //TODO 开始执行转账操作
            try {
                do {
                    DB::beginTransaction();
                    //TODO 扣除当前账号余额
                    $update = Agent::on('mysql')->find($this->_auth->id)->update([
                        'wallet' => floatval($agent->wallet - $data['money'])
                    ]);

                    if ($update) {
                        //添加下线钱包
                        $updateUnder = Agent::on('mysql')->where('id', $this->_auth->id)->update([
                            'wallet' => floatval($underAgent->wallet + $data['money'])
                        ]);

                        if ($updateUnder) {
                            logRecord("转账 agentName {$this->_auth->agentName} 成功修改了余额");
                            logRecord("转账 agentName {$data['username']} 成功修改了余额");

                            //TODO 添加转账记录
                            AgentTransferLog::on('mysql')->create([
                                'agentId' => $this->_auth->id,
                                'toAgentId' => $underAgent->id,
                                'money' => $data['money'],
                                'beforeAgentMoney' => floatval($agent->wallet),
                                'afterAgentMoney' => floatval($agent->wallet - $data['money']),
                                'beforeMemberMoney' => floatval($underAgent->wallet),
                                'afterMemberMoney' => floatval($underAgent->wallet + $data['money']),
                            ]);

                            DB::commit();
                            return $this->successResponse();
                        }
                    }
                } while(0);
                DB::rollBack();
            } catch (\Exception $e) {
                logRecord($e->getMessage());
                return $this->errorResponse('代理转账失败', [
                    'info' => $e->getMessage()
                ]);
            }
        }

        return $this->errorResponse('转账失败');
    }

    /**
     * 2.8 修改代理信息
     *
     * @bodyParam id int 代理ID. Example: 1
     * @bodyParam realName string 真实姓名【为空才能修改】.
     * @bodyParam phone int 手机号码【为空才能修改】.
     * @bodyParam email string 邮箱【为空才能修改】.
     * @bodyParam qq string QQ【为空才能修改】.
     * @bodyParam wechat string 微信号【为空才能修改】.
     * @bodyParam birthDay date 生日:YYYY-MM-DD【为空才能修改】.
     *
     * @response{
     *       "message": "处理成功",
     *       "code": 0,
     *       "data": []
     *   }
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AgentUpdateRequest $request)
    {
        $id = $request->get('id', $this->_auth->id);
        //TODO 判断当前是否传入了id
        if ($id <= 0) {
            return $this->errorResponse('缺少代理信息');
        }

        $agent = Agent::on('mysql_slave')->find($id)->where('status', 1);

        if (empty($agent)) {
            return $this->errorResponse('获取代理信息失败');
        }

        $realName = $request->get('realName', '');

        $updateAgent = [];
        if (empty($agent->realName) && !empty($realName)) {
            $updateAgent['realName'] = doSecret($realName, 'username');
        }

        $phone = $request->get('phone', '');
        if (empty($agent->phone) && !empty($phone)) {
            $updateAgent['phone'] = doSecret($phone, 'phone');
        }

        $email = $request->get('email', '');
        if (empty($agent->email) && !empty($email)) {
            $updateAgent['email'] = doSecret($email, 'email');
        }

        if (!empty($updateAgent)) {
            $update = Agent::on('mysql')->where('id', $this->_auth->id)->update($updateAgent);
            if (!$update) {
                return $this->errorResponse('更新代理信息失败');
            }
        }

        $agentInfo = AgentInfo::on('mysql_slave')->where('agentId', $id)->first();

        $updateInfo = [];

        $qq = $request->get('qq', '');
        $wechat = $request->get('wechat', '');
        $birthDay = $request->get('birthDay', '');
        if (empty($agentInfo)) {
            $updateInfo['agentId'] = $id;

            if (!empty($qq)) {
                $updateInfo['qq'] = $qq;
            }

            if (!empty($wechat)) {
                $updateInfo['wechat'] = $wechat;
            }

            if (!empty($birthDay)) {
                $updateInfo['birthDay'] = $birthDay;
            }

            if (!empty($updateInfo)) {
                $update = AgentInfo::on('mysql')->create($updateInfo);
                if (!$update) {
                    return $this->errorResponse('更新代理信息失败');
                }
            }
        } else {
            if (empty($agentInfo->qq) && !empty($qq)) {
                $updateInfo['qq'] = $qq;
            }

            if (empty($agentInfo->wechat) && !empty($wechat)) {
                $updateInfo['wechat'] = $wechat;
            }

            if (empty($agentInfo->birthDay) && !empty($birthDay)) {
                $updateInfo['birthDay'] = $birthDay;
            }

            if (!empty($updateInfo)) {
                $update = AgentInfo::on('mysql')->where('agentId', $id)->update($updateInfo);
                if (!$update) {
                    return $this->errorResponse('更新代理信息失败');
                }
            }
        }

        return $this->successResponse();
    }

    /**
     * 2.9 转账记录
     *
     * @queryParam beginTime string 开始时间. Example: YYYY-MM-DD HH:ii:ss
     * @queryParam endTime string 结束时间. Example: YYYY-MM-DD HH:ii:ss
     * @queryParam page int 当前页数. Example: 1
     * @queryParam pageSize int 每页数量. Example: 15
     *
     * @response{
     *       "message": "处理成功",
     *       "code": 0,
     *       "data": [
     *          "id" : "id",
     *          "agentId" : "发起人",
     *          "toAgentId" : "代理ID",
     *          "toMemberId" : "用户ID",
     *          "money" : "转账金额",
     *          "created_at" : "转账时间",
     *          "username" : "用户名"
     *       ]
     *   }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function transferLog(Request $request)
    {
        $query = AgentTransferLog::on('mysql_slave')->getQuery();

        $query->where('agentId', $this->_auth->id);

        //TODO 判断查询时间 是否为当天
        $beginTime = $request->get('beginTime', '');

        if (empty($beginTime)) {
            $beginTime = date('Y-m-d 00:00:00');
        }

        $endTime = $request->get('endTime', '');

        if (empty($endTime)) {
            $endTime = date('Y-m-d H:i:s');
        }

        $timeRange = timeRangeSql($beginTime, $endTime, 45);

        $query->whereBetween('created_at', [$timeRange['beginTime'], $timeRange['endTime']]);

        //TODO 获取总数
        $agentCount = $query->count();

        if ($agentCount > 0) {
            //分页参数
            $nowPage = $request->get('page', 1);

            $pageSize = $request->get('pageSize', 15);

            //TODO 分页
            $pagenation = getTotalPage($agentCount, (int)$nowPage, (int)$pageSize);

            $list = $query
                ->select('id', 'agentId', 'toAgentId', 'toAgentName', 'toMemberId', 'toMemberName', 'money', 'created_at')
                ->offset($pagenation['page'])
                ->limit($pagenation['limit'])
                ->get()
                ->toArray();

            $data = [];
            foreach ($list as $val) {
                $val->money = amountFilter($val->money, 2);
                $val->username = $val->toAgentId ? $val->toAgentName : $val->toMemberName;
                $data[] = $val;
            }

            return $this->successResponse([
                'list' => $data,
                'count' => $agentCount,
                'totalPage' => $pagenation['total']
            ]);
        }

        return $this->successNullResponse();
    }

    /****************************  私有方法 ***********************/
    //创建邀请码
    private function _createInviteCode() : string
    {
        //TODO 判断代理邀请码是否重复 -- 查主库
        $inviteCode = createOrderNo('A');
        $count = Agent::on('mysql')->where('inviteCode', $inviteCode)->count();
        if ($count <= 0) {
            return $inviteCode;
        }

        //TODO 添加日志
        logRecord("_createInviteCode exists {$inviteCode}");
        $this->_createInviteCode();
    }
}
