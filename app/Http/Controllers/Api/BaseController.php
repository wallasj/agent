<?php
declare(strict_types=1);
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;

class BaseController extends Controller
{

    public $_auth;

    public function __construct()
    {
        try {
            $this->_auth = auth('api')->user();
            //TODO REDIS
        } catch (\Exception $e) {
        }
    }

    //
    public function errorResponse(string $message = "处理失败", array $data = [], int $code = -1)
    {
        return response()->json([
            'message' => $message,
            'code' => $code,
            'data' => $data,
        ]);
    }

    public function successResponse($data = [], string $message = "处理成功", int $code = 0)
    {
        if (is_null($data)) $data = [];
        return response()->json([
            'message' => $message,
            'code' => $code,
            'data' => $data,
        ]);
    }

    public function successNullResponse(string $message = "处理成功", int $code = 0)
    {
        return response()->json([
            'message' => $message,
            'code' => $code,
            'data' => [
                'list' => [],
                'count' => 0,
                'totalPage' => 0
            ],
        ]);
    }


    //请求API - 透传
    public function _getApi(string $method, string $url, array $data = [], string $return = 'json')
    {
        if (empty($data)) {
            $data = \request()->all();
        }

        //TODO 调用接口
        try {
            $client = new \GuzzleHttp\Client();
            $curl = $client->request($method, $url, [
                'headers' => [
                    'Agentsys' => config('api.memberApi.secret')
                ],
                'query' => $data
            ]);
            $code = $curl->getStatusCode();
            if ($code == 200) {
                $result = $curl->getBody()->getContents();
                if (!empty($result)) {
                    if ($return == 'array') return $result;
                    if (!is_null(json_decode($result))) {
                        return response()->json(json_decode($result));
                    } else {
                        return $this->successResponse($result);
                    }
                }
            } else {
                logRecord('接口请求失败 CODE : ' . $code, 'error');
            }
        } catch (GuzzleException $e) {
            logRecord('接口请求失败 CURL : ' . $e->getMessage(), 'error');
        }
        return $this->errorResponse('接口请求失败');
    }

}
