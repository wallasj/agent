<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\Commission;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Api\Member\MemberController;
use App\Http\Resources\Api\CommissionCollection;
use App\Model\Agent;
use App\Model\AgentCommissionStatistics;
use App\Model\AgentDayReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group 4.佣金模块
 * Class CommissionController
 * @package App\Http\Controllers\Api\Commission
 */
class CommissionController extends BaseController
{

    /**
     * 4.1 佣金报表
     * @authenticated
     *
     * @queryParam page int 当前页数. Example: 1
     * @queryParam pageSize int 每页数量. Example: 15
     *
     * @response {
     *   "message": "处理成功",
     *   "code": 0,
     *   "data": {
     *       "list" : [{
     *              "id": "",
     *              "agentId": "",
     *              "proposalNo": 0,
     *              "commissionTime": 0,
     *              "commissionName": 0,
     *              "commissionAmount": 0,
     *              "status": 0,
     *              "statusText": 0,
     *              "beginTime": 0,
     *              "endTime": 0
     *           },{
     *              "id": "",
     *              "agentId": "",
     *              "proposalNo": 0,
     *              "commissionTime": 0,
     *              "commissionName": 0,
     *              "commissionAmount": 0,
     *              "status": 0,
     *              "statusText": 0,
     *              "beginTime": 0,
     *              "endTime": 0
     *          }],
     *       "count": 1,
     *       "totalPage": 1
     *      }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function report(Request $request)
    {
        $query = AgentCommissionStatistics::on('mysql_slave')->getQuery();

        $query->where('agentId', $this->_auth->id);

        //TODO 获取总数
        $commissionCount = $query->count();

        if ($commissionCount > 0) {
            //分页参数
            $nowPage = $request->get('page', 1);

            $pageSize = $request->get('pageSize', 15);

            //TODO 分页
            $pagenation = getTotalPage($commissionCount, (int)$nowPage, (int)$pageSize);

            //查询当前用户所有的佣金提案
            $commission = $query
                ->select('id', 'agentId', 'proposalNo', 'commissionTime', 'commissionName', 'commissionAmount', 'status', 'beginTime', 'endTime')
                ->offset($pagenation['page'])
                ->limit($pagenation['limit'])
                ->orderBy('created_at', 'desc')
                ->get();

            if (!$commission->isEmpty()) {
                //TODO 格式转换
                $commission = CommissionCollection::collection($commission);
                $list = $commission->toArray($request);

                return $this->successResponse([
                    'list' => $list,
                    'count' => $commissionCount,
                    'totalPage' => $pagenation['total']
                ]);
            }
        }

        return $this->successNullResponse();
    }

    /**
     * 4.2 子代理报表
     * @authenticated
     *
     * @queryParam agentName string 代理姓名.
     * @queryParam registerStartTime string 注册开始时间. Example: YYYY-MM-DD HH:ii:ss
     * @queryParam registerEndTime string 注册结束时间. Example: YYYY-MM-DD HH:ii:ss
     * @queryParam page int 当前页数. Example: 1
     * @queryParam pageSize int 每页数量. Example: 15
     *
     * @response {
     *   "message": "处理成功",
     *   "code": 0,
     *   "data":  {
     *       "list" : [{
     *                  "id": "",
     *                  "agentName": "",
     *                  "depositAmount": 0,
     *                  "withdrawAmount": 0,
     *                  "promoteAmount": 0,
     *                  "betAmount": 0,
     *                  "betValidAmount": 0,
     *                  "payoutAmountdaili": 0
     *                },{
     *                  "id": "",
     *                  "agentName": "",
     *                  "depositAmount": 0,
     *                  "withdrawAmount": 0,
     *                  "promoteAmount": 0,
     *                  "betAmount": 0,
     *                  "betValidAmount": 0,
     *                  "payoutAmountdaili": 0
     *                }]
     *          "count" : 2,
     *          "totalPage": 1
     *      }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function agentReport(Request $request)
    {
        $type = $request->get('type', '');

        $agentName = $request->get('agentName', '');

        $registerStartTime = $request->get('registerStartTime', '');

        $registerEndTime = $request->get('registerEndTime', '');

        if (empty($registerStartTime)) {
            $registerStartTime = date('Y-m-d 00:00:00', strtotime('-1 day', time()));
        }

        if (empty($registerEndTime)) {
            $registerEndTime = date('Y-m-d 23:59:59', strtotime('-1 day', time()));
        }

        $timeRange = timeRangeSql($registerStartTime, $registerEndTime, 45);

        $agentId = $this->_auth->id;
        $left = $this->_auth->left;
        $right = $this->_auth->right;
        if (!empty($agentName) && $this->_auth->agentName != $agentName) {
            //获取当前人员的agentId
            $agent = Agent::on('mysql_slave')
                ->select('id', 'left', 'right')
                ->where('parent_id', $agentId)
                ->where('left', '>', $left)
                ->where('right', '<', $right)
                ->where('status', 1)
                ->where('agentName', $agentName)
                //->whereBetween('created_at', [$timeRange['beginTime'], $timeRange['endTime']])
                ->first();
            if ($agent) {
                $agentId = $agent->id;
                $left = $agent->left;
                $right = $agent->right;
            } else {
                return $this->successNullResponse();
            }
        }

        if ($type == 'self') {
            $searchIds[] = $agentId;

            $children = $this->__getAgentChildren($agentId, $left, $right, $timeRange, 'list');

            if (!$children->isEmpty()) {
                foreach ($children as $val) {
                    $searchIds[] = $val->id;
                }
            }

            $result = AgentDayReport::on('mysql_slave')
                ->select(
                    DB::raw('SUM(depositAmount) as depositAmount'),
                    DB::raw('SUM(withdrawAmount) as withdrawAmount'),
                    DB::raw('SUM(promoteAmount) as promoteAmount'),
                    DB::raw('SUM(betAmount) as betAmount'),
                    DB::raw('SUM(betValidAmount) as betValidAmount'),
                    DB::raw('SUM(payoutAmount) as payoutAmount')
                )
                ->whereIn('agentId', $searchIds)
                ->first();

            if ($result) {
                $result['id'] = $agentId;
                $result['agentName'] = $agentName ? $agentName : $this->_auth->agentName;
                $commissionCollection = new CommissionCollection($result);
                $result = $commissionCollection->agentReport();

                return $this->successResponse([
                    'list' => $result,
                    'count' => 1,
                    'totalPage' => 1
                ]);
            }
        } else {
            $count = $this->__getAgentChildren($agentId, $left, $right, $timeRange,'count');

            if ($count > 0) {
                $rdata = [];

                //分页参数
                $nowPage = $request->get('page', 1);

                $pageSize = $request->get('pageSize', 15);

                //TODO 分页
                $pagenation = getTotalPage($count, (int)$nowPage, (int)$pageSize);

                $children = Agent::on('mysql_slave')
                    ->select('id', 'agentName')
                    //->where('parent_id', $agentId)
                    ->where('left', '>', $left)
                    ->where('right', '<', $right)
                    ->where('status', 1)
                    ->whereBetween('created_at', [$timeRange['beginTime'], $timeRange['endTime']])
                    ->offset($pagenation['page'])
                    ->limit($pagenation['limit'])
                    ->get();

                if (!$children->isEmpty()) {
                    foreach ($children as $val) {
                        $result = AgentDayReport::on('mysql_slave')
                            ->select(
                                DB::raw('SUM(depositAmount) as depositAmount'),
                                DB::raw('SUM(withdrawAmount) as withdrawAmount'),
                                DB::raw('SUM(promoteAmount) as promoteAmount'),
                                DB::raw('SUM(betAmount) as betAmount'),
                                DB::raw('SUM(betValidAmount) as betValidAmount'),
                                DB::raw('SUM(payoutAmount) as payoutAmount')
                            )
                            ->where('agentId', $val->id)
                            ->whereBetween('created_at', [$timeRange['beginTime'], $timeRange['endTime']])
                            ->first();
                        if ($result) {
                            $result['id'] = $agentId;
                            $result['agentName'] = $val->agentName;
                            $commissionCollection = new CommissionCollection($result);
                            $rdata[] = $commissionCollection->agentReport();
                        }
                    }
                }

                return $this->successResponse([
                    'list' => $rdata,
                    'count' => $count,
                    'totalPage' => $pagenation['total']
                ]);
            }
        }

        return $this->successNullResponse();
    }

    /**
     * 4.3 代理日报表
     * @authenticated
     *
     * @queryParam type string 查看类型.【self 自己/children 所有下线】.
     * @queryParam agentName string 代理姓名.
     * @queryParam beginTime string 开始时间. Example: YYYY-MM-DD HH:ii:ss
     * @queryParam endTime string 结束时间. Example: YYYY-MM-DD HH:ii:ss
     * @queryParam page int 当前页数. Example: 1
     * @queryParam pageSize int 每页数量. Example: 15
     *
     * @response {
     *   "message": "处理成功",
     *   "code": 0,
     *   "data":  {
     *       "list" : [{
     *                  "id": "",
     *                  "agentName": "",
     *                  "registerCount": 0,
     *                  "totalAccount": 0,
     *                  "totalEarnings": 0,
     *                  "totalSingle": 0,
     *                  "depositAmount": 0,
     *                  "withdrawAmount": 0,
     *                  "dwRate": 0,
     *                  "promoteAmount": 0,
     *                  "betAmount": 0,
     *                  "betValidAmount": 0,
     *                  "payoutAmount": 0
     *                },{
     *                  "id": "",
     *                  "agentName": "",
     *                  "registerCount": 0,
     *                  "totalAccount": 0,
     *                  "totalEarnings": 0,
     *                  "totalSingle": 0,
     *                  "depositAmount": 0,
     *                  "withdrawAmount": 0,
     *                  "dwRate": 0,
     *                  "promoteAmount": 0,
     *                  "betAmount": 0,
     *                  "betValidAmount": 0,
     *                  "payoutAmount": 0
     *                }]
     *          "count" : 2,
     *          "totalPage": 1
     *      }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function dailyReport(Request $request)
    {
        $type = $request->get('type', '');

        $agentName = $request->get('agentName', '');

        $beginTime = $request->get('beginTime', '');

        if (empty($beginTime)) {
            $beginTime = date('Y-m-d 00:00:00', strtotime('-1 day', time()));
        }

        $endTime = $request->get('endTime', '');

        if (empty($endTime)) {
            $endTime = date('Y-m-d 23:59:59', strtotime('-1 day', time()));
        }

        $timeRange = timeRangeSql($beginTime, $endTime, 45);

        $agentId = $this->_auth->id;
        $left = $this->_auth->left;
        $right = $this->_auth->right;
        if (!empty($agentName) && $this->_auth->agentName != $agentName) {
            //获取当前人员的agentId
            $agent = Agent::on('mysql_slave')
                ->select('id', 'left', 'right')
                ->where('parent_id', $agentId)
                ->where('left', '>', $left)
                ->where('right', '<', $right)
                ->where('status', 1)
                ->where('agentName', $agentName)
                //->whereBetween('created_at', [$timeRange['beginTime'], $timeRange['endTime']])
                ->first();
            if ($agent) {
                $agentId = $agent->id;
                $left = $agent->left;
                $right = $agent->right;
            } else {
                return $this->successNullResponse();
            }
        }

        if ($type == 'self') {
            $searchIds[] = $agentId;

            $children = $this->__getAgentChildren($agentId, $left, $right, $timeRange, 'list');

            if (!$children->isEmpty()) {
                foreach ($children as $val) {
                    $searchIds[] = $val->id;
                }
            }

            $result = AgentDayReport::on('mysql_slave')
            ->select(
                DB::raw('SUM(registerCount) as registerCount'),
                DB::raw('SUM(totalAccount) as totalAccount'),
                DB::raw('SUM(totalEarnings) as totalEarnings'),
                DB::raw('SUM(totalSingle) as totalSingle'),
                DB::raw('SUM(depositAmount) as depositAmount'),
                DB::raw('SUM(withdrawAmount) as withdrawAmount'),
                DB::raw('SUM(dwRate) as dwRate'),
                DB::raw('SUM(promoteAmount) as promoteAmount'),
                DB::raw('SUM(betAmount) as betAmount'),
                DB::raw('SUM(betValidAmount) as betValidAmount'),
                DB::raw('SUM(payoutAmount) as payoutAmount')
            )
            ->whereBetween('created_at', [$timeRange['beginTime'], $timeRange['endTime']])
            ->whereIn('agentId', $searchIds)
            ->first();

            if ($result) {
                $result['id'] = $agentId;
                $result['agentName'] = $agentName ? $agentName : $this->_auth->agentName;
                $commissionCollection = new CommissionCollection($result);
                $result = $commissionCollection->dailyReport();

                return $this->successResponse([
                    'list' => $result,
                    'count' => 1,
                    'totalPage' => 1
                ]);
            }
        } else {
            $rdata = [];

            $searchIds[] = $agentId;

            $children = $this->__getAgentChildren($agentId, $left, $right, $timeRange, 'list');

            if (!$children->isEmpty()) {
                foreach ($children as $val) {
                    $searchIds[] = $val->id;
                }
            }

            $result = AgentDayReport::on('mysql_slave')
                ->select(
                    DB::raw('SUM(registerCount) as registerCount'),
                    DB::raw('SUM(totalAccount) as totalAccount'),
                    DB::raw('SUM(totalEarnings) as totalEarnings'),
                    DB::raw('SUM(totalSingle) as totalSingle'),
                    DB::raw('SUM(depositAmount) as depositAmount'),
                    DB::raw('SUM(withdrawAmount) as withdrawAmount'),
                    DB::raw('SUM(dwRate) as dwRate'),
                    DB::raw('SUM(promoteAmount) as promoteAmount'),
                    DB::raw('SUM(betAmount) as betAmount'),
                    DB::raw('SUM(betValidAmount) as betValidAmount'),
                    DB::raw('SUM(payoutAmount) as payoutAmount')
                )
                ->whereBetween('created_at', [$timeRange['beginTime'], $timeRange['endTime']])
                ->whereIn('agentId', $searchIds)
                ->first();

            if ($result) {
                $result['id'] = $agentId;
                $result['agentName'] = $agentName ? $agentName : $this->_auth->agentName;
                $commissionCollection = new CommissionCollection($result);
                $rdata[] = $commissionCollection->dailyReport();
            }

            $count = $this->__getAgentChildren($agentId, $left, $right, $timeRange,'count');

            if ($count > 0) {

                //分页参数
                $nowPage = $request->get('page', 1);

                $pageSize = $request->get('pageSize', 15);

                //TODO 分页
                $pagenation = getTotalPage($count, (int)$nowPage, (int)$pageSize);

                $children = Agent::on('mysql_slave')
                    ->select('id', 'agentName')
                    //->where('parent_id', $agentId)
                    ->where('left', '>', $left)
                    ->where('right', '<', $right)
                    ->where('status', 1)
                    //->whereBetween('created_at', [$timeRange['beginTime'], $timeRange['endTime']])
                    ->offset($pagenation['page'])
                    ->limit($pagenation['limit'])
                    ->get();

                if (!$children->isEmpty()) {
                    foreach ($children as $val) {
                        $result = AgentDayReport::on('mysql_slave')
                            ->select(
                                DB::raw('SUM(registerCount) as registerCount'),
                                DB::raw('SUM(totalAccount) as totalAccount'),
                                DB::raw('SUM(totalEarnings) as totalEarnings'),
                                DB::raw('SUM(totalSingle) as totalSingle'),
                                DB::raw('SUM(depositAmount) as depositAmount'),
                                DB::raw('SUM(withdrawAmount) as withdrawAmount'),
                                DB::raw('SUM(dwRate) as dwRate'),
                                DB::raw('SUM(promoteAmount) as promoteAmount'),
                                DB::raw('SUM(betAmount) as betAmount'),
                                DB::raw('SUM(betValidAmount) as betValidAmount'),
                                DB::raw('SUM(payoutAmount) as payoutAmount')
                            )
                            ->where('agentId', $val->id)
                            ->whereBetween('created_at', [$timeRange['beginTime'], $timeRange['endTime']])
                            ->first();
                        if ($result) {
                            $result['id'] = $agentId;
                            $result['agentName'] = $val->agentName;
                            $commissionCollection = new CommissionCollection($result);
                            $rdata[] = $commissionCollection->dailyReport();
                        }
                    }
                }
            }

            return $this->successResponse([
                'list' => $rdata,
                'count' => $count,
                'totalPage' => $pagenation['total'] ?? 1
            ]);
        }

        return $this->successNullResponse();
    }

    /**
     * 4.4 输赢报表
     * @authenticated
     *
     * @queryParam type string 查看类型.【self 自己/children 所有下线】.
     * @queryParam agentName string 代理姓名.
     * @queryParam beginTime string 开始时间. Example: YYYY-MM-DD HH:ii:ss
     * @queryParam endTime string 结束时间. Example: YYYY-MM-DD HH:ii:ss
     * @queryParam page int 当前页数. Example: 1
     * @queryParam pageSize int 每页数量. Example: 15
     *
     * @response {
     *   "message": "处理成功",
     *   "code": 0,
     *   "data":  {
     *       "list" : [{
     *                  "id": "",
     *                  "agentName": "",
     *                  "depositAmount": 0,
     *                  "withdrawAmount": 0,
     *                  "totalSingle": 0,
     *                  "betAmount": 0,
     *                  "betValidAmount": 0,
     *                  "promoteAmount": 0,
     *                  "payoutAmount": 0,
     *                  "winningsAmount": 0
     *                },{
     *                  "id": "",
     *                  "agentName": "",
     *                  "depositAmount": 0,
     *                  "withdrawAmount": 0,
     *                  "totalSingle": 0,
     *                  "betAmount": 0,
     *                  "betValidAmount": 0,
     *                  "promoteAmount": 0,
     *                  "payoutAmount": 0,
     *                  "winningsAmount": 0
     *                }]
     *          "count" : 2,
     *          "totalPage": 1
     *      }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function winOrLoseReport(Request $request)
    {
        //TODO 判断查询时间 是否为当天
        $beginTime = $request->get('beginTime', '');

        if (empty($beginTime)) {
            $beginTime = date('Y-m-d 00:00:00');
        }

        $endTime = $request->get('endTime', '');

        if (empty($endTime)) {
            $endTime = date('Y-m-d H:i:s');
        }

        $type = $request->get('type', '');

        $agentName = $request->get('agentName', '');

        $timeRange = timeRangeSql($beginTime, $endTime, 45);

        // 是否仅仅查询自己
        $agentId = $this->_auth->id;
        $left = $this->_auth->left;
        $right = $this->_auth->right;
        if (!empty($agentName) && $this->_auth->agentName != $agentName) {
            //获取当前人员的agentId
            $agent = Agent::on('mysql_slave')
                ->select('id', 'left', 'right')
                ->where('parent_id', $agentId)
                ->where('left', '>', $left)
                ->where('right', '<', $right)
                ->where('status', 1)
                ->where('agentName', $agentName)
                //->whereBetween('created_at', [$timeRange['beginTime'], $timeRange['endTime']])
                ->first();
            if ($agent) {
                $agentId = $agent->id;
                $left = $agent->left;
                $right = $agent->right;
            } else {
                return $this->successNullResponse();
            }
        }

        // 聚合
        if ($type == 'self') {
            //获取所有下线代理
            $searchIds[] = $agentId;

            $children = $this->__getAgentChildren($agentId, $left, $right, $timeRange,'list');

            if (!$children->isEmpty()) {
                foreach ($children as $val) {
                    $searchIds[] = $val->id;
                }
            }

            $result = $this->__getAgentStatisticsById($searchIds, $timeRange);

            if ($result) {
                $result['id'] = $agentId;
                $result['agentName'] = $agentName ? $agentName : $this->_auth->agentName;
                return $this->successResponse([
                    'list' => $result,
                    'count' => 1,
                    'totalPage' => 1
                ]);
            }
        }
        else {
            $count = $this->__getAgentChildren($agentId, $left, $right, $timeRange, 'count');

            //分页参数
            $nowPage = $request->get('page', 1);

            $pageSize = $request->get('pageSize', 15);

            //TODO 分页
            $pagenation = getTotalPage($count, (int)$nowPage, (int)$pageSize);

            $children = Agent::on('mysql_slave')
                ->select('id', 'agentName')
                //->where('parent_id', $agentId)
                ->where('left', '>', $left)
                ->where('right', '<', $right)
                ->where('status', 1)
                ->whereBetween('created_at', [$timeRange['beginTime'], $timeRange['endTime']])
                ->offset($pagenation['page'])
                ->limit($pagenation['limit'])
                ->get();

            $rdata = [];
            if (!$children->isEmpty()) {
                foreach ($children as $val) {
                    $result = $this->__getAgentStatisticsById([$val->id], $timeRange);
                    if ($result) {
                        $result['id'] = $val->id;
                        $result['agentName'] = $val->agentName;
                        $rdata[] = $result;
                    }
                }
            }
            return $this->successResponse([
                'list' => $rdata,
                'count' => $count,
                'totalPage' => $pagenation['total']
            ]);
        }

        //查询当前操作者下线 - 列表
        return $this->successNullResponse();
    }

    /**
     * 4.5 佣金明细
     * @authenticated
     *
     * @queryParam id required int 代理ID.
     *
     * @response {
     *   "message": "处理成功",
     *   "code": 0,
     *   "data": {
     *           "id": "",
     *           "beginTime": "佣金开始时间",
     *           "endTime": "佣金结算时间",
     *           "wallet": "账户余额",
     *           "commissionName": "佣金名称",
     *           "commissionAmount": "本期佣金",
     *           "status": "佣金状态",
     *           "statusText": "状态文字",
     *           "lastSurplus": "上期结余",
     *           "actualCommission": "实际可领佣金",
     *           "payoutAmount": "总派彩",
     *           "profitAmount": "总盈利",
     *           "promoteAmout": "总优惠",
     *           "offlineCost": "线下运营成本",
     *           "bankRate": "银行卡手续费",
     *           "otherCost": "其他费用",
     *           "remark": ""
     *       }
     *   }
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function commissionDetail(Request $request)
    {
        $id = $request->get('id', 0);

        if (is_numeric($id) && intval($id) > 0) {
            //查询当前用户所有的佣金提案
            $commission = AgentCommissionStatistics::on('mysql_slave')
                ->where('agentId', $this->_auth->id)
                ->where('id', $id)
                ->first();
            if (!empty($commission)) {
                //TODO 格式转换
                $commission = new CommissionCollection($commission);
                $detail = $commission->details();

                return $this->successResponse($detail);
            }
        }
        return $this->errorResponse('未查询到佣金明细');
    }

    //获取代理统计
    private function __getAgentStatisticsById(array $agentId, array $timeRange)
    {
        if (!empty($agentId)) {
            $agentId = implode(',', $agentId);
            try {
                $member = new MemberController();
                $data = $member->getDayStatistics($agentId, $timeRange);
                if ($data) {
                    $data = json_decode($data);
                    return (array)$data->data;
                }
            } catch(\Exception $e) {
            }
        }
        return [];
    }

    private function __getAgentChildren($agentId, $left, $right, $timeRange, $type = 'list')
    {
        if ($type == 'list') {
            return Agent::on('mysql_slave')
                ->select('id', 'agentName')
                //->where('parent_id', $agentId)
                ->where('left', '>', $left)
                ->where('right', '<', $right)
                ->where('status', 1)
                //->whereBetween('created_at', [$timeRange['beginTime'], $timeRange['endTime']])
                ->get();
        } else {
            return Agent::on('mysql_slave')
                //->where('parent_id', $agentId)
                ->where('left', '>', $left)
                ->where('right', '<', $right)
                ->where('status', 1)
                //->whereBetween('created_at', [$timeRange['beginTime'], $timeRange['endTime']])
                ->count();
        }
    }

}
