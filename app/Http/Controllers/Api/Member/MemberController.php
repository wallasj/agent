<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\Member;

use App\Console\Commands\agentDayReport;
use App\Http\Controllers\Api\BaseController;
use Illuminate\Http\Request;

/**
 * @group 3.会员模块
 * Class MemberController
 * @package App\Http\Controllers\Api\Member
 */
class MemberController extends BaseController
{

    /**
     * 3.1 下线会员列表
     * @authenticated
     *
     * @queryParam source string 来源. Example: pc/h5/ios/android
     * @queryParam beginTime string 开始时间. Example: YYYY-MM-DD HH:ii:ss
     * @queryParam endTime string 结束时间. Example: YYYY-MM-DD HH:ii:ss
     * @queryParam page int 当前页数. Example: 1
     * @queryParam pageSize int 每页数量. Example: 15
     *
     * @response {
     *   "message": "处理成功",
     *   "code": 0,
     *   "data": {
     *          "list": [
     *              {
     *                  "id": "20",
     *                  "realname": "嘿嘿",
     *                  "nickname": null,
     *                  "levelId": "1",
     *                  "registerSite": "127.0.0.1",
     *                  "firstDepositDate": null,
     *                  "registerTime": "2019-12-05 14:46:18"
     *              }
     *          ],
     *          "count": 1,
     *          "totalPage": 1
     *      }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function memberList(Request $request)
    {
        $data = $request->all();
        $data['agentId'] = $this->_auth->id;
        return $this->_getApi('GET', (string)config('api.memberApi.list'), $data);
    }

    /**
     * 3.2 添加下线会员
     * @authenticated
     *
     * @bodyParam username required string 用户名.
     * @bodyParam password required string 密码.
     * @bodyParam payword required string 支付密码.
     * @bodyParam realname required string 真实姓名.
     * @bodyParam gender int 性别【0女 1男】.
     * @bodyParam telephone int 用户手机号码.
     * @bodyParam qq int QQ.
     * @bodyParam wechat string 微信.
     * @bodyParam email string 用户邮箱.
     * @bodyParam birthday string 用户生日:【YYYY-MM-DD HH:ii:ss】.
     *
     * @response {
     *   "message": "处理成功",
     *   "code": 0,
     *   "data": 0
     *}
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addMember(Request $request)
    {
        $data = $request->all();
        $data['agentId'] = $this->_auth->id;
        $data['agentName'] = $this->_auth->agentName;
        return $this->_getApi('POST', (string)config('api.memberApi.add'), $data);
    }

    /**
     * 3.3 下线会员明细
     * @authenticated
     *
     * @queryParam id required int 所查询的会员ID. Example: 1
     * @queryParam beginTime string 开始时间. Example: YYYY-MM-DD HH:ii:ss
     * @queryParam endTime string 结束时间. Example: YYYY-MM-DD HH:ii:ss
     *
     * @response {
     *   "message": "处理成功",
     *   "code": 0,
     *   "data": {
     *      "id": "5",
     *      "realname": "贝恩",
     *      "nickname": "jh2",
     *      "qq": "QQ",
     *      "wechat": "微信",
     *      "gender": "性别",
     *      "birthday": "生日",
     *      "email": "邮箱",
     *      "telephone": "电话",
     *      "registerTime": "2019-11-20 18:50:38",
     *      "loginTime": "2019-11-20 18:50:38",
     *      "depositAmount": 0,
     *      "withdrawAmount": 0,
     *      "betValidAmount": 0,
     *      "payoutAmount": 0,
     *      "promoteAmount": 0,
     *      "winningsAmount": 0
     *  }
     * }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function memberDetail(Request $request)
    {
        //TODO 调用接口
        $data = $request->all();
        $data['agentId'] = $this->_auth->id;
        return $this->_getApi('GET', (string)config('api.memberApi.detail'), $data);
    }

    /**
     * 3.4 修改下线会员
     * @authenticated
     *
     * @bodyParam memberId required int 用户ID.
     * @bodyParam realname string 真实姓名[为空时可修改].
     * @bodyParam gender int 性别[0女 1男].
     * @bodyParam telephone int 用户手机号码[为空时可修改].
     * @bodyParam qq int QQ[为空时可修改].
     * @bodyParam wechat string 微信[为空时可修改].
     * @bodyParam email string 用户邮箱[为空时可修改].
     * @bodyParam birthday string 用户生日:[YYYY-MM-DD].
     *
     * @response {
     *   "message": "处理成功",
     *   "code": 0,
     *   "data": 0
     *}
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMember(Request $request)
    {
        $data = $request->all();
        $data['agentId'] = $this->_auth->id;
        return $this->_getApi('GET', (string)config('api.memberApi.update'), $data);
    }

    public function getMemberStatistics(int $agentId=0)
    {
        if ($agentId <= 0) $agentId = $this->_auth->id;
        $data['agentId'] = $agentId;
        return $this->_getApi('GET', (string)config('api.memberApi.memberStatistics'), $data, 'array');
    }

    public function getMmeberByName(string $memberName)
    {
        $data['agentId'] = $this->_auth->id;
        $data['username'] = $memberName;
        return $this->_getApi('GET', (string)config('api.memberApi.memberInfoByName'), $data, 'array');
    }

    public function transferMoney(int $memberId, float $money)
    {
        $data['agentId'] = $this->_auth->id;
        $data['id'] = $memberId;
        $data['money'] = $money;
        return $this->_getApi('GET', (string)config('api.memberApi.memberTransfer'), $data, 'array');
    }

    public function getDayStatistics(string $agentId = "", array $timeRange)
    {
        if ($agentId == "") $agentId = $this->_auth->id;
        $data['agentId'] = $agentId;
        $data['beginTime'] = $timeRange['beginTime'] ?? date('Y-m-d 00:00:00');
        $data['endTime'] = $timeRange['endTime'] ?? date('Y-m-d H:i:s');
        return $this->_getApi('GET', (string)config('api.memberApi.memberDayStatistics'), $data, 'array');
    }

    public function getCommissionStatistics(string $agentId = "", array $timeRange)
    {
        if ($agentId == "") $agentId = $this->_auth->id;
        $data['agentId'] = $agentId;
        $data['beginTime'] = $timeRange['beginTime'] ?? date('Y-m-d 00:00:00');
        $data['endTime'] = $timeRange['endTime'] ?? date('Y-m-d H:i:s');
        return $this->_getApi('GET', (string)config('api.memberApi.memberCommissionStatistics'), $data, 'array');
    }

}
