<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\Letter;

use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\Api\LetterCollection;
use App\Model\Letter;
use Illuminate\Http\Request;

/**
 * @group 5.站内信模块
 * Class LetterController
 * @package App\Http\Controllers\Api\Letter
 * 站内信
 */
class LetterController extends BaseController
{

    /**
     * 5.1 站内信列表
     *
     * @queryParam page int 当前页数. Example: 1
     * @queryParam pageSize int 每页数量. Example: 15
     *
     * @apiResourceCollection App\Http\Resources\Api\LetterCollection
     * @apiResourceModel App\Model\Letter
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function letterList(Request $request)
    {
//        $query = Letter::on('mysql_slave')->getQuery();
//
//        //TODO 获取总数
//        $letterCount = $query->count();
//
//        if ($letterCount > 0) {
//            //分页参数
//            $nowPage = $request->get('page', 1);
//
//            $pageSize = $request->get('pageSize', 15);
//
//            //TODO 分页
//            $pagenation = getTotalPage($letterCount, (int)$nowPage, (int)$pageSize);
//
//            //TODO 查询条件
//            $letter = $query
//                ->offset($pagenation['page'])
//                ->limit($pagenation['limit'])
//                ->get();
//            if (!$letter->isEmpty()) {
//                //TODO 设置REDIS
//                $letter = LetterCollection::collection($letter);
//                $list = $letter->toArray($request);
//
//                return $this->successResponse([
//                    'list' => $list,
//                    'count' => $letterCount,
//                    'totalPage' => $pagenation['total']
//                ]);
//            }
//        }

        $list = [];
        $letterCount = 0;
        $totalPage = 0;
        try{
            $getList = $this->_getApi('GET', (string)config('api.memberApi.noticeList'), [], 'array');
            $data = json_decode($getList, true);
            if ($data['code'] == 0) {
                $list = $data['data']['list'];
                foreach ($list as $key => $val) {
                    $list[$key]['content'] = strip_tags($val['content']);
                }
                $letterCount = $data['data']['count'];
                $totalPage = $data['data']['totalPage'];
            }
        } catch(\Exception $e) {
            logRecord('error', $e->getMessage());
        }

        return $this->successResponse([
            'list' => $list,
            'count' => $letterCount,
            'totalPage' => $totalPage
        ]);
    }

    /**
     * 5.2 获取站内信总数
     * 目前逻辑按照一周的消息数来取
     * @response {
     *   "message": "处理成功",
     *   "code": 0,
     *   "data": 0
     *}
     * @return \Illuminate\Http\JsonResponse
     */
    public function letterCount()
    {
//        $query = Letter::on('mysql_slave')->getQuery();
//
//        $query->whereBetween('created_at', [timeRange('weekStart'), nowDate()]);
//
//        //TODO 获取总数
//        $letterCount = $query->count();
        return $this->successResponse(0);

//        $letterCount = 0;
//        try{
//            $getCount = $this->_getApi('GET', (string)config('api.memberApi.noticeCount'), [], 'array');
//            $data = json_decode($getCount, true);
//            if ($data['code'] == 0) {
//                $letterCount = $data['data']['count'];
//            }
//        } catch(\Exception $e) {
//            logRecord('error', $e->getMessage());
//        }
//        return $this->successResponse($letterCount);
    }

}
