<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AgentPwdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(\Auth::user() == null) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => [
                'required',
                Rule::in(['password', 'payword'])
            ],
            'oldPassword' => 'required|max:12',
            'newPassword' => 'required|max:12|required_with:confirmPassword|same:confirmPassword',
            'confirmPassword' => 'required|max:12',
            'code' => 'numeric|digits_between:4,6',
        ];
    }

    public function attributes()
    {
        return [
            //必填 、 长度判断 、 格式
            'type' => '密码类型',
            'oldPassword' => '旧密码',
            'newPassword' => '新密码',
            'confirmPassword' => '确认密码',
            'code' => '验证码'
        ];
    }
}
