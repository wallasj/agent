<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class AgentTransferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //必填 、 长度判断 、 格式
            'username' => 'required|max:30',
            'payword' => 'required|max:12',
            'money' => 'required|numeric|min:0.01'
        ];
    }

    public function attributes()
    {
        return [
            //必填 、 长度判断 、 格式
            'username' => '用户名',
            'money' => '转账金额',
            'payword' => '支付密码'
        ];
    }

}
