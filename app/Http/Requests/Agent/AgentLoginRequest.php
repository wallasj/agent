<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class AgentLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'agentName' => 'required|max:30',
            'password' => 'required|max:12',
        ];
    }

    public function attributes()
    {
        return [
            //必填 、 长度判断 、 格式
            'agentName' => '代理账号',
            'password' => '代理密码'
            //'inviteCode' => '代理推广码'
        ];
    }
}
