<?php

namespace App\Http\Requests\Agent;

use App\Rules\ValidatePhoneRule;
use Illuminate\Foundation\Http\FormRequest;

class AgentRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //必填 、 长度判断 、 格式
            'agentNo' => 'required|unique:agent|max:32',
            'agentName' => 'required|unique:agent|max:30',
            'password' => 'required|max:12',
            'realName' => 'required|max:15',
            'phone' => ['required', new ValidatePhoneRule, 'max:13'],
            'email' => 'unique:agent|email|max:30',
            'inviteCode' => 'alpha_num',
            'qq' => 'numeric|digits_between:5,15',
            'wechat' => 'alpha_num|max:30',
            'birthDay' => 'date',
            'status' => 'numeric|between:-1,0'
        ];
    }

    public function attributes()
    {
        return [
            //必填 、 长度判断 、 格式
            'agentNo' => '代理编号',
            'agentName' => '代理账号',
            'password' => '代理密码',
            'realName' => '真实姓名',
            'phone' => '手机号码',
            'email' => '邮箱地址l',
            'inviteCode' => '代理推广码',
            'qq' => 'QQ号码',
            'wechat' => '微信',
            'birthDay' => '生日'
        ];
    }

}
