<?php

namespace App\Http\Requests\Agent;

use App\Rules\ValidatePhoneRule;
use Illuminate\Foundation\Http\FormRequest;

class AgentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //必填 、 长度判断 、 格式
            'id' => 'numeric',
            'realName' => 'max:15',
            'phone' => [new ValidatePhoneRule, 'max:13'],
            'email' => 'email|max:30',
            'inviteCode' => 'alpha_num',
            'qq' => 'numeric|digits_between:5,15',
            'wechat' => 'alpha_num|max:30',
            'birthDay' => 'date'
        ];
    }

    public function attributes()
    {
        return [
            //必填 、 长度判断 、 格式
            'id' => '代理ID',
            'realName' => '真实姓名',
            'phone' => '手机号码',
            'email' => '邮箱地址l',
            'inviteCode' => '代理推广码',
            'qq' => 'QQ号码',
            'wechat' => '微信',
            'birthDay' => '生日'
        ];
    }

}
