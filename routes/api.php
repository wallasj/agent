<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//代理前端API
Route::namespace('Api')->prefix('v1')->group(function () {

    //基础模块
    Route::namespace('Base')->group(function(){
        //代理登录
        Route::post('login', 'LoginController@login');

        Route::middleware('auth:api')->group(function(){
            //查看登录信息 - 暂不开放
            Route::get('info', 'LoginController@info');

            //刷新token
            Route::post('refresh', 'LoginController@refresh');

            //退出登录
            Route::post('out', 'LoginController@loginOut');
        });

    });

    //代理模块
    Route::namespace('Agent')->prefix('agent')->group(function(){
        //注册代理 / 添加子代理
        Route::post('register', 'AgentController@store');

        //获取代理号
        Route::get('invite_code', 'AgentController@inviteCode');

        //查看代理推广码是否已经使用
        Route::get('check_agent', 'AgentController@checkAgentBy');

        Route::middleware('auth:api')->group(function(){
            //修改代理信息
            Route::post('update', 'AgentController@update');

            //查看下线代理
            Route::get('list', 'AgentController@agentList');

            //查看代理信息
            Route::get('detail', 'AgentController@show');

            //修改代理密码
            Route::post('change_password', 'AgentController@changePwd');

            //转账
            Route::post('transfer', 'AgentController@transferMoney');

            //转代记录
            Route::get('transfer_log', 'AgentController@transferLog');
        });
    });

    //会员模块
    Route::namespace('Member')->prefix('member')->group(function(){

        Route::middleware('auth:api')->group(function() {
            //走接口
            //查看下线会员
            Route::get('list', 'MemberController@memberList');

            //添加下线会员
            Route::post('add_member', 'MemberController@addMember');

            //查看下线会员 - 明细
            Route::get('detail', 'MemberController@memberDetail');

            //修改下线会员
            Route::post('update_member', 'MemberController@updateMember');
        });

    });

    //站内信
    Route::namespace('Letter')->prefix('letter')->group(function(){

        //站内信列表
        Route::get('list', 'LetterController@letterList');

        //站内信未读总数
        Route::get('count', 'LetterController@letterCount');

    });

    //佣金模块
    Route::namespace('Commission')->prefix('commission')->group(function(){

        Route::middleware('auth:api')->group(function() {
            //佣金报表
            Route::get('commission_report', 'CommissionController@report');

            //代理报表
            Route::get('agent_report', 'CommissionController@agentReport');

            //代理日报表
            Route::get('daily_report', 'CommissionController@dailyReport');

            //输赢报表
            Route::get('wl_report', 'CommissionController@winOrLoseReport');

            //佣金明细
            Route::get('commission_detail', 'CommissionController@commissionDetail');
        });

    });

    //银行卡
    Route::namespace('Bank')->prefix('bank')->group(function(){

        //银行卡列表
        Route::get('list', 'BankController@bankList');

        Route::middleware('auth:api')->group(function() {

            //获取当前用户银行卡列表
            Route::get('agentBank', 'BankController@agentBankList');

            //获取当前用户银行卡信息
            Route::get('agentBankInfo', 'BankController@agentBankInfo');

            //获取当前用户银行卡信息
            Route::post('addAgentBank', 'BankController@addAgentBank');

            //获取当前用户银行卡信息
            Route::post('delAgentBank', 'BankController@delAgentBank');

        });

    });

    //取款
    Route::namespace('Withdraw')->prefix('withdraw')->group(function(){

        Route::middleware('auth:api')->group(function() {

            //创建提现记录
            Route::post('do', 'WithdrawController@add');

            //取款记录
            Route::get('log', 'WithdrawController@withDrawLog');

        });

    });

});
